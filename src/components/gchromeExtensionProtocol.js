const Cc = Components.classes;
const Ci = Components.interfaces;

const nsISupports = Ci.nsISupports;
const nsIIOService = Ci.nsIIOService;
const nsIProtocolHandler = Ci.nsIProtocolHandler;
const nsIStandardURL = Ci.nsIStandardURL;

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

function gchromeExtensionProtocol() {
  Components.utils.import("resource://googlechromeextensions/googlechromeengine.jsm");
  Components.utils.import("resource://googlechromeextensions/services/CONSOLE.jsm");
  this._googleChromeEngine = googlechromeengine;
  this._console = CONSOLE;
  this._chromeRegistry = Components.classes['@mozilla.org/chrome/chrome-registry;1'].getService();
  this._chromeRegistry = this._chromeRegistry.QueryInterface(Components.interfaces.nsIChromeRegistry);
  this._ioService = Components.classes["@mozilla.org/network/io-service;1"].getService(Components.interfaces.nsIIOService)

}

gchromeExtensionProtocol.prototype = {
  scheme : "gchrome-extension",
  classDescription: "google chrome extension Protocol",
  classID:          Components.ID("{f5499e7b-582b-491c-91fb-868f688990c5}"),
  contractID:       "@mozilla.org/network/protocol;1?name=gchrome-extension",
  defaultPort: 80,
  protocolFlags: nsIProtocolHandler.URI_STD | nsIProtocolHandler.URI_LOADABLE_BY_ANYONE,

  allowPort: function(port, scheme)
  {
    return false;
  },

  newURI: function(spec, charset, baseURI)
  {
    var uri = Cc["@mozilla.org/network/standard-url;1"].createInstance(nsIStandardURL);
    uri.init(nsIStandardURL.URLTYPE_STANDARD, 80, spec, "UTF-8", baseURI);
    return uri;
  },

  newChannel: function(aURI)
  {
    var url = '';
    var feature = aURI.spec;
    var extension = this._googleChromeEngine.findExtension(aURI.host);
    if (extension != null) {
      url = "chrome://googlechromeextensions_extensions/content/" + aURI.host + aURI.path;
    }
    else {
      url = "chrome://googlechromeextensions/content/notfound";
    }

    var uri = this._ioService.newURI(url, null, null);
    var convertedUri = this._chromeRegistry.convertChromeURL(uri);
    var channel = this._ioService.newChannel(
        convertedUri.spec,
        null,
        null
        );
    channel.originalURI = aURI;
    return channel;
  },

  QueryInterface: XPCOMUtils.generateQI([nsISupports, nsIProtocolHandler])
}

function NSGetModule(compMgr, fileSpec) {
  return XPCOMUtils.generateModule([gchromeExtensionProtocol]);
}