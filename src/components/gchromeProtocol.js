const Cc = Components.classes;
const Ci = Components.interfaces;

const nsISupports = Ci.nsISupports;
const nsIIOService = Ci.nsIIOService;
const nsIProtocolHandler = Ci.nsIProtocolHandler;
const nsIStandardURL = Ci.nsIStandardURL;

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

function gchromeProtocol() {
  Components.utils.import("resource://googlechromeextensions/googlechromeengine.jsm");
  this._googleChromeEngine = googlechromeengine;

}

gchromeProtocol.prototype = {
  scheme : "gchrome",
  classDescription: "google chrome Protocol",
  classID:          Components.ID("{1825291d-8d5c-4c55-9152-be8a0c0117ab}"),
  contractID:       "@mozilla.org/network/protocol;1?name=gchrome",
  defaultPort: 80,
  protocolFlags: nsIProtocolHandler.URI_STD | nsIProtocolHandler.URI_LOADABLE_BY_ANYONE,

  allowPort: function(port, scheme)
  {
    return false;
  },

  newURI: function(spec, charset, baseURI)
  {
    var uri = Cc["@mozilla.org/network/standard-url;1"].createInstance(nsIStandardURL);
    uri.init(nsIStandardURL.URLTYPE_AUTHORITY, 80, spec, "UTF-8", baseURI);
    return uri;
  },

  newChannel: function(aURI)
  {
    var url = '';
    var feature = aURI.spec;
    if (aURI.host == "extensions"){
      url = "chrome://googlechromeextensions/content/extensions/extensions.html";
    }
    else {
      url = "chrome://googlechromeextensions/content/notfound";
    }
    var ios = Cc["@mozilla.org/network/io-service;1"].getService(nsIIOService);
    var channel = ios.newChannel(
        url,
        null,
        null
        );

    channel.originalURI = aURI;
    
    return channel;

  },

  QueryInterface: XPCOMUtils.generateQI([nsISupports, nsIProtocolHandler])
}

function NSGetModule(compMgr, fileSpec) {
  return XPCOMUtils.generateModule([gchromeProtocol]);
}