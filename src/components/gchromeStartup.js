const Cc = Components.classes;
const Ci = Components.interfaces;

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

const gObserver = Cc['@mozilla.org/observer-service;1'].getService(Ci.nsIObserverService);

function gchromeStartup() {
}

gchromeStartup.prototype = {
  classDescription: "gchrome Startup",
  classID:          Components.ID("{c4ab5c54-9849-4bda-bb03-06f1064e4fb6}"),
  contractID:       "@matoeil.fr/gchrome-startup;1",

  observe: function(aSubject, aTopic, aData) {
    switch (aTopic) {
      case "app-startup":
        gObserver.addObserver(this, "xpcom-shutdown", false);
        gObserver.addObserver(this, "final-ui-startup", false);
        break;
      case "xpcom-shutdown":
        gObserver.removeObserver(this, "xpcom-shutdown");
        gObserver.removeObserver(this, "final-ui-startup");
        break;
      case "final-ui-startup":
        Components.utils.import("resource://googlechromeextensions/googlechromeengine.jsm");
        break;
    }
  },

  QueryInterface: XPCOMUtils.generateQI([Components.interfaces.nsISupports, Components.interfaces.nsIObserver]),

  _xpcom_categories: [
    {
      category: "app-startup",
      service: true
    }
  ]
}

function NSGetModule(compMgr, fileSpec) {
  return XPCOMUtils.generateModule([gchromeStartup]);
}