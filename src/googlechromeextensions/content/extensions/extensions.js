Components.utils.import("resource://googlechromeextensions/googlechromeengine.jsm");

function requestExtensionsData() {
	var extensions = googlechromeengine.getExtensions();
	var count = 0;
	var extensionTemplate = document.getElementById("extension-template");
	var viewTemplate = document.getElementById("view-template");
	var extensionsList = document.getElementById("extensionsList");
	for (var id in extensions) {
		var extension = extensions[id];
		count++;
		var extensionView = extensionTemplate.cloneNode(true);
		extensionView.removeAttribute("id");
		extensionView.style.display = null;

		//TODO isEnabled
		var extensionEnabledState = extensionView.querySelector(".extension-enabled-state");
		extensionEnabledState.className = "extension-enabled-state extension_enabled";

		var extensionIcon = extensionView.querySelector(".extension-icon");
		var icon
		if ((icon = extension.getIcon48()) != null) {
			extensionIcon.src = extension.getExtensionUrl() + icon;
		}
		else if ((icon = extension.getIcon128()) != null) {
			extensionIcon.src = extension.getExtensionUrl() + icon;
		}
		else {
			extensionIcon.src = "chrome://googlechromeextensions/content/extensions/no-icon.png";
		}

		var extensionName = extensionView.querySelector(".extension-name");
		extensionName.textContent = extension.getName();

		var extensionVersion = extensionView.querySelector(".extension-version");
		extensionVersion.textContent = extension.getVersion().getVersionString();


		var extensionDescription = extensionView.querySelector(".extension-description");
		extensionDescription.textContent = extension.getDescription();

		var extensionID = extensionView.querySelector(".extension-id");
		extensionID.textContent = extension.getID();

		var extensionViews = extensionView.querySelector(".extension-views");
		var views = extension.getViews().getViewsAsArray();

		if (views.length == 0) {
			var extensionDetails = extensionView.querySelector(".extension-details-view");
			extensionDetails.style.display = "none";
		}
		else {
			for (var i = 0, l = views.length; i < l; i++) {
				var view = views[i];
				var viewView = viewTemplate.cloneNode(true);
				viewView.removeAttribute("id");
				var viewAction = viewView.querySelector(".view-action");
				var viewName = viewView.querySelector(".view-name");
				viewName.textContent = view.getLocation();
				viewAction.addEventListener("click", function(aDocument){
					return function(){
							sendInspectMessage(aDocument);
							return true;
					}
				}(view.getChromeWindow().document), false)
				extensionViews.appendChild(viewView);
			}
		}

		var extensionActions = extensionView.querySelector(".extension-actions");
		extensionActions.setAttribute("extensionId", extension.getID());

		var optionPage;
		var extensionOptionsPageButton = extensionView.querySelector(".extension-options-cell button");
		if ((optionPage = extension.getOptionsPage()) != null	/* && TODO isEnabled*/) {
			extensionOptionsPageButton.setAttribute("extensionId", extension.getID());
		}
		else {
			extensionOptionsPageButton.setAttribute("disabled", "true");
		}


		extensionsList.appendChild(extensionView);
	}
	var extensionsCount = document.getElementById("extensions-count");
	var noExtension = document.getElementById("no-extension");
	if (count > 0) {
		extensionsCount.textContent = "(" + count + ")";
		noExtension.style.display = "none";
	}
	else {
		extensionsCount.style.display = "none";
	}

}

function handleOptions(anElement) {
  var extension = googlechromeengine.findExtension(anElement.getAttribute("extensionId"));
  var optionPage = null;
  if ((optionPage = extension.getOptionsPage()) != null	/* && TODO isEnabled*/) {
    window.open(extension.getExtensionUrl()+extension.getOptionsPage(), "_blank", "");
  }
}

function handleReloadExtension(anElement) {
	alert(anElement.parentNode.getAttribute("extensionId"));
}

function handleEnableExtension(anElement, aIsEnabled) {
	alert(anElement.parentNode.getAttribute("extensionId") + " / " + aIsEnabled);
}

function handleUninstallExtension(anElement) {
	alert(anElement.parentNode.getAttribute("extensionId"));
}

function sendInspectMessage(aDocument){
	window.openDialog("chrome://inspector/content/", "_blank", "chrome,all,dialog=no", aDocument);
}

function noHref(){
  return null;
}