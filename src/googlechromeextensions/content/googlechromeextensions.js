function showExtensions() {
	var url = "gchrome://extensions/";
	var found = false;
	var tabbrowser = gBrowser;

	// Check each tab of this browser instance
	var numTabs = tabbrowser.browsers.length;
	for (var index = 0; index < numTabs; index++) {
		var currentBrowser = tabbrowser.getBrowserAtIndex(index);
		if (url == currentBrowser.currentURI.spec) {

			// The URL is already opened. Select this tab.
			tabbrowser.selectedTab = tabbrowser.tabContainer.childNodes[index];

			// Focus *this* browser-window
			browserWin.focus();

			found = true;
			break;
		}
	}


	// Our URL isn't open. Open it now.
	if (!found) {
		gBrowser.selectedTab = gBrowser.addTab(url);
	}
}