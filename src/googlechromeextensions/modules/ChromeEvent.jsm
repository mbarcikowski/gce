Components.utils.import("resource://googlechromeextensions/services/CONSOLE.jsm");

var EXPORTED_SYMBOLS = ["ChromeEvent"];

function ChromeEvent() {
  this._listenerEntries = new Array();
}

ChromeEvent.prototype._console = CONSOLE;

ChromeEvent.prototype.init = function init() {
  return this;
}

ChromeEvent.prototype.notify = function notify(aSenderView, aArgs) {
  var listenerEntries = this._listenerEntries.slice(0, this._listenerEntries.length);
  for (var i = 0, l = listenerEntries.length; i < l; i++) {
    try {
      var entry = listenerEntries[i];
      if (aSenderView != entry.view) {
        entry.listener.apply(entry.view, aArgs);
      }
    }
    catch(e) {
      this._console.error(e);
    }
  }
}

ChromeEvent.prototype.callbackNotify = function notify(aSenderView, aCallback) {
  var listenerEntries = this._listenerEntries.slice(0, this._listenerEntries.length);
  var isNotify = false;
  for (var i = 0, l = listenerEntries.length; i < l; i++) {
    try {
      var entry = listenerEntries[i];
      if (aSenderView != entry.view) {
        var args = aCallback(entry.view);
        if (args != null) {
          isNotify = true;
          entry.listener.apply(entry.listener, args);
        }
      }
    }
    catch(e) {
      this._console.error(e);
    }
  }
  return isNotify;
}


ChromeEvent.prototype._addListener = function addListener(aView, aListener) {
  if (!this._hasListener(aListener)) {
    var entry = {view: aView, listener: aListener};
    this._listenerEntries.push(entry);
  }
}

ChromeEvent.prototype._removeListener = function removeListener(aListener) {
  for (var i = 0, l = this._listenerEntries.length; i < l; i++) {
    if (this._listenerEntries[i].listener == aListener) {
      this._listenerEntries.splice(i, 1);
      return;
    }
  }
}

ChromeEvent.prototype._hasListener = function hasListener(aListener) {
  for (var i = 0, l = this._listenerEntries.length; i < l; i++) {
    if (this._listenerEntries[i].listener == aListener) {
      return true;
    }
  }
  return false;
}

ChromeEvent.prototype._hasListeners = function _hasListeners() {
  return this._listenerEntries.length > 0;
}

ChromeEvent.prototype.initWrapper = function initWrapper(aView) {
  var wrapper = {
    addListener : function(aChromeEvent, aInnerView) {
      return function(aListener) {
        return aChromeEvent._addListener(aInnerView, aListener);
      }
    }(this, aView),
    removeListener : function(aChromeEvent) {
      return function(aListener) {
        return aChromeEvent._removeListener(aListener);
      }
    }(this),
    hasListener : function(aChromeEvent) {
      return function(aListener) {
        return aChromeEvent._hasListener(aListener);
      }
    }(this),
    hasListeners : function(aChromeEvent) {
      return function() {
        return aChromeEvent._hasListeners();
      }
    }(this)
  };

  return wrapper;
}

ChromeEvent.prototype.releaseWrapper = function releaseWrapper(aView) {
  var newListenerEntries = new Array();
  for (var i = 0, l = this._listenerEntries.length; i < l; i++) {
    var entry = this._listenerEntries[i];
    if (aView != entry.view) {
      newListenerEntries.push(entry);
    }
  }
  this._listenerEntries = newListenerEntries;
}

ChromeEvent.prototype.finalize = function finalize() {
  this._listenerEntries = new Array();
}