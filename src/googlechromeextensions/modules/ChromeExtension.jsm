Components.utils.import("resource://googlechromeextensions/services/JSON.jsm");
Components.utils.import("resource://googlechromeextensions/services/HIDDENWINDOW.jsm");
Components.utils.import("resource://googlechromeextensions/services/CONSOLE.jsm");
Components.utils.import("resource://googlechromeextensions/services/WINDOWS.jsm");
Components.utils.import("resource://googlechromeextensions/services/TABS.jsm");
Components.utils.import("resource://googlechromeextensions/services/FILE.jsm");
Components.utils.import("resource://googlechromeextensions/Version.jsm");
Components.utils.import("resource://googlechromeextensions/Matcher.jsm");
Components.utils.import("resource://googlechromeextensions/ContentScriptRule.jsm");
Components.utils.import("resource://googlechromeextensions/api/i18n/I18n.jsm");
Components.utils.import("resource://googlechromeextensions/api/extension/BackgroundPage.jsm");
Components.utils.import("resource://googlechromeextensions/api/browseraction/BrowserAction.jsm");
Components.utils.import("resource://googlechromeextensions/api/tabs/Tabs.jsm");
Components.utils.import("resource://googlechromeextensions/api/windows/Windows.jsm");
Components.utils.import("resource://googlechromeextensions/api/extension/Extension.jsm");
Components.utils.import("resource://googlechromeextensions/Views.jsm");

var EXPORTED_SYMBOLS = ["ChromeExtension"];


function ChromeExtension() {
  this._id = null;
  this._browserAction = null;
  this._version = null;
  this._minimum_chrome_version = null;
  this._i18n = null;
  this._name = null;
  this._description = null;
  this._backgroundPage = null;
  this._windows = null;
  this._tabs = null;
  this._extension = null;
  this._views = null;
  this._icon48 = null;
  this._icon128 = null;
  this._optionsPage = null;
  this._contentScriptRules = new Array();
  this._extensionDirectory = null;
  this._tabsPermission = false;
  this._bookmarksPermission = false;
  this._matchesPermissions = new Array();
}

ChromeExtension.prototype._schemePattern = "^([A-Za-z]+)://";
ChromeExtension.prototype._extensionScheme = "gchrome-extension://";

ChromeExtension.prototype.init = function init(aExtensionDirectory, aManifestContent) {
  this._extensionDirectory = aExtensionDirectory;
  this._views = new Views().init(this);
  this._id = aExtensionDirectory.leafName;
  try {
    var manifest = JSON.decode(aManifestContent);
  }
  catch(e) {
    throw new Error("Manifest is not a valid JSON.");
  }
  if (manifest == null) {
    throw new Error("Manifest is not a valid JSON.");
  }

  this._i18n = new I18n();
  this._i18n.init(aExtensionDirectory, manifest);


  if (manifest.name == null || (manifest.name + "").length > 45) {
    throw new Error("Required value 'name' is missing or invalid.");
  }
  else {
    manifest.name = manifest.name + "";
    this._name = this._i18n.parseManifestString(manifest.name);
  }

  try {
    this._version = new Version().init(manifest.version);
  }
  catch(e) {
    throw new Error("Required value 'version' is missing or invalid. " + e.message);
  }

  if (manifest.theme != null) {
    throw new Error("Themes are not supported.");
  }

  if (manifest.description != null && (manifest.description + "").length > 132) {
    throw new Error("Optional value 'description' is invalid.");
  }
  else {
    manifest.description = manifest.description + "";
    this._description = this._i18n.parseManifestString(manifest.description);
  }

  if (manifest.icons != null) {
    for (var size in manifest.icons) {
      var sizeStr = size + "";
      switch (sizeStr) {
        case "48":
          this._icon48 = manifest.icons[size];
          break;
        case "128":
          this._icon128 = manifest.icons[size];
          break;
        default :
      }
    }
  }


  if (manifest.minimum_chrome_version != null) {
    try {
      this._minimum_chrome_version = new Version().init(manifest.minimum_chrome_version);
    }
    catch(e) {
      throw new Error("Optional value 'minimum_chrome_version' is invalid. " + e.message);
    }
    //TODO check _minimum_chrome_version
  }

  if (manifest.browser_action != null && manifest.page_action != null) {
    throw new Error("An extension cannot have both a page action and a browser action.");
  }

  this._browserAction = new BrowserAction().init(this);
  if (manifest.browser_action != null) {
    if (manifest.browser_action.default_icon != null) {
      manifest.browser_action.default_icon = manifest.browser_action.default_icon + "";
      if (manifest.browser_action.default_icon.length == 0) {
        throw new Error("Invalid value for 'browser_action.default_icon'.");
      }
      try {
        FILE.getFile(aExtensionDirectory, manifest.browser_action.default_icon);
      }
      catch(e) {
        throw new Error("Could not load icon '" + manifest.browser_action.default_icon + "' for browser action.");
      }
    }
    this._browserAction.setActive(true);
    this._browserAction.setIconPath(this.convertToExtensionUrl(manifest.browser_action.default_icon));
    this._browserAction.setTitle(this._i18n.parseManifestString(manifest.browser_action.default_title));
    this._browserAction.setPopup(this.convertToExtensionUrl(manifest.browser_action.popup));
  }

  if (manifest.page_action != null) {
    if (manifest.page_action.default_icon != null) {
      manifest.page_action.default_icon = manifest.page_action.default_icon + "";
      if (manifest.page_action.default_icon.length == 0) {
        throw new Error("Invalid value for 'page_action.default_icon'.");
      }
      try {
        FILE.getFile(aExtensionDirectory, manifest.page_action.default_icon);
      }
      catch(e) {
        throw new Error("Could not load icon '" + manifest.page_action.default_icon + "' for page action.");
      }
      //TODO  this.manifest.page_action.default_title = this._i18n.parseManifestString(manifest.page_action.default_title);
    }
  }

  this._backgroundPage = new BackgroundPage().init(this);
  if (manifest.background_page != null) {
    manifest.background_page = manifest.background_page + "";
    try {
      FILE.getFile(aExtensionDirectory, manifest.background_page);
    }
    catch(e) {
      throw new Error("Could not load background page '" + manifest.background_page + "'.");
    }
    this._backgroundPage.setBackgroundPage(manifest.background_page);
  }

  if (manifest.chrome_url_overrides != null) {
    for (var override in manifest.chrome_url_overrides) {
      if ((override + "") != "newtab") {
        throw new Error("Invalid value for 'chrome_url_overrides'.");
      }
    }
    //TODO chrome_url_overrides
  }

  if (manifest.content_scripts != null) {
    if (!(manifest.content_scripts instanceof Array)) {
      throw new Error("Invalid value for 'content_scripts'.");
    }
    for (var i_scripts = 0, l_scripts = manifest.content_scripts.length; i_scripts < l_scripts; i_scripts ++) {
      var contentScriptRule = new ContentScriptRule().init();
      var content_script = manifest.content_scripts[i_scripts];

      var run_at = content_script.run_at;
      if (run_at != null) {
        run_at = run_at + "";
        switch (run_at) {
          case "document_start" :
          case "document_end" :
          case "document_idle" :
            contentScriptRule.setRunAt(run_at);
            break;
          default :
            throw new Error("Invalid value for 'content_scripts[" + i_scripts + "].run_at'.");
        }

      }
      else {
        contentScriptRule.setRunAt("document_idle");
      }

      var all_frames = content_script.all_frames;
      if (all_frames != null) {
        if (!('boolean' === typeof all_frames)) {
          throw new Error("Invalid value for 'content_scripts[" + i_scripts + "].all_frames'.");
        }
        contentScriptRule.setAllFrames(all_frames);
      }
      else {
        contentScriptRule.setAllFrames(false);
      }

      if (content_script.matches == null || !(content_script.matches instanceof Array)) {
        throw new Error("Invalid value for 'content_scripts[" + i_scripts + "].matches' is missing or invalid.");
      }
      if (content_script.matches.length == 0) {
        throw new Error("Invalid value for 'content_scripts[" + i_scripts + "].matches'. There must be at least one match specified.");
      }
      for (var i_matches = 0, l_matches = content_script.matches.length; i_matches < l_matches; i_matches++) {
        try {
          var match = new Matcher().init(content_script.matches[i_matches]);
        }
        catch(e) {
          throw new Error("Invalid value for 'content_scripts[" + i_scripts + "].matches[" + i_matches + "]'.");
        }
        contentScriptRule.addMatch(match);
      }
      if (content_script.js == null && content_script.css == null) {
        throw new Error("At least one js or css file is required for 'content_scripts[" + i_scripts + "]'.");
      }
      if (content_script.js != null) {
        if (!(content_script.js instanceof Array)) {
          throw new Error("Invalid value for 'content_scripts[" + i_scripts + "].js' is invalid.");
        }
        for (var i_javascripts = 0, l_javascripts = content_script.js.length; i_javascripts < l_javascripts; i_javascripts++) {
          var js = content_script.js[i_javascripts] + "";
          try {
            //TODO maybe cache file?
            FILE.getFile(aExtensionDirectory, js);
          }
          catch(e) {
            throw new Error("Could not load javascript '" + js + "' for content script.");
          }
          contentScriptRule.addJS(js)
        }
      }
      if (content_script.css != null) {
        if (!(content_script.css instanceof Array)) {
          throw new Error("Invalid value for 'content_scripts[" + i_scripts + "].css' is invalid.");
        }
        for (var i_csses = 0, l_csses = content_script.css.length; i_csses < l_csses; i_csses++) {
          var css = content_script.css[i_csses] + "";
          try {
            //TODO maybe cache file?
            FILE.getFile(aExtensionDirectory, css);
          }
          catch(e) {
            throw new Error("Could not load css '" + css + "' for content script.");
          }
          contentScriptRule.addCSS(css)
        }
      }
      this._contentScriptRules.push(contentScriptRule);
    }
  }

  if (manifest.options_page != null) {
    manifest.options_page = manifest.options_page + "";
    this._optionsPage = manifest.options_page;
  }

  if (manifest.permissions != null) {
    if (!(manifest.permissions instanceof Array)) {
      throw new Error("Required value 'permissions' is missing or invalid.");
    }
    for (var i_permissions = 0, l_permissions = manifest.permissions.length; i_permissions < l_permissions; i_permissions++) {
      if (manifest.permissions[i_permissions] == null) {
        throw new Error("Invalid value for 'permissions[" + i_permissions + "]'.");
      }
      var permission = manifest.permissions[i_permissions] + "";
      try {
        switch (permission) {
          case "tabs":
            this._tabsPermission = true;
            break;
          case "bookmarks":
            this._bookmarksPermission = true;
            break;
          default :
            this._matchesPermissions.push(new Matcher().init(permission));
        }
      }
      catch(e) {
        throw new Error("Invalid value for 'permissions[" + i_permissions + "]'.");
      }
    }
    if (this._tabsPermission) {
      //TODO verify if init is needed
      this._tabs = new Tabs().init(this);
      this._windows = new Windows().init(this);
    }
    //TODO bookmarks persmission
  }

  if (manifest.plugins != null) {
    throw new Error("Plugins are not supported.");
  }

  if (manifest.update_url != null) {
    manifest.update_url = manifest.update_url + "";
    //TODO update_url
  }

  this._extension = new Extension().init(this);
  return this;
}
ChromeExtension.prototype.getID = function getID() {
  return this._id;
}

ChromeExtension.prototype.getExtensionUrl = function() {
  return this._extensionScheme + this._id + "/";
}

ChromeExtension.prototype.getIcon48 = function() {
  return this._icon48;
}

ChromeExtension.prototype.getIcon128 = function() {
  return this._icon128;
}


//TODO as test is extension path
ChromeExtension.prototype.convertToExtensionUrl = function convertToExtensionUrl(aPath) {
  if (aPath != null) {
    try {
      var schemeRegExp = new RegExp(this._schemePattern, "g");
      if (schemeRegExp.test(aPath)) {
        return aPath;
      }
      else {
        if (aPath.indexOf(this.getExtensionUrl()) == 0) {
          return aPath;
        }
        else {
          return this.getExtensionUrl() + aPath;
        }
      }
    }
    catch(e) {
      CONSOLE.error(e);
      return null;
    }
  }
  else {
    return null;
  }
}

ChromeExtension.prototype.initChromeWrapper = function initChromeWrapper(aView) {
  var wrapper = {};
  wrapper.browserAction = this._browserAction.initWrapper(aView);
  if (this._windows != null && this._tabsPermission) {
    wrapper.windows = this._windows.initWrapper(aView);
  }
  else {
    wrapper.windows = undefined;
  }
  if (this._tabs != null && this._tabsPermission) {
    wrapper.tabs = this._tabs.initWrapper(aView);
  }
  else {
    wrapper.tabs = undefined;
  }
  wrapper.extension = this._extension.initWrapper(aView);
  wrapper.i18n = this._i18n.getWrapper();
  if (this._bookmarksPermission) {
    //TODO bookmarks
    wrapper.bookmarks = undefined;
  }
  else {
    wrapper.bookmarks = undefined;
  }
  return wrapper;
}

ChromeExtension.prototype.releaseChromeWrapper = function releaseChromeWrapper(aView) {
  this._browserAction.releaseWrapper(aView);
  if (this._windows != null) {
    this._windows.releaseWrapper(aView);
  }
  if (this._tabs != null) {
    this._tabs.releaseWrapper(aView);
  }
  this._extension.releaseWrapper(aView);
}


ChromeExtension.prototype.getName = function getName() {
  return this._name;
}

ChromeExtension.prototype.getDescription = function getDescription() {
  return this._description;
}

ChromeExtension.prototype.getVersion = function getVersion() {
  return this._version;
}

ChromeExtension.prototype.getOptionsPage = function getOptionsPage() {
  return this._optionsPage;
}

ChromeExtension.prototype.getViews = function() {
  return this._views;
}

ChromeExtension.prototype.getLocalStorage = function getLocalStorage() {
  return HIDDENWINDOW.getLocalStorage(this);
}

ChromeExtension.prototype.getBackgroundPage = function getBackgroundPage() {
  return this._backgroundPage;
}

ChromeExtension.prototype.getBrowserAction = function getBrowserAction() {
  return this._browserAction;
}

ChromeExtension.prototype.getTabs = function getTabs() {
  return this._tabs;
}

ChromeExtension.prototype.getWindows = function getWindows() {
  return this._windows;
}

ChromeExtension.prototype.getExtension = function getExtension() {
  return this._extension;
}

ChromeExtension.prototype.loadBrowserAction = function loadBrowserAction(aWindow) {
  if (this._browserAction.isActive()) {
    this._browserAction.createBrowserActionElement(aWindow);
  }
}

ChromeExtension.prototype.getDataUrl = function getDataUrl(aImageData) {
  return HIDDENWINDOW.getDataUrl(aImageData);
}

ChromeExtension.prototype.getSnapshot = function (aChromeWindow) {
  return HIDDENWINDOW.getSnapshot(aChromeWindow);
}

ChromeExtension.prototype.openWindow = function (aUrl, aLeft, aTop, aWidth, aHeight) {
  return HIDDENWINDOW.openWindow(this.convertToExtensionUrl(aUrl), aLeft, aTop, aWidth, aHeight);
}

ChromeExtension.prototype.getContentScriptRules = function getContentScriptRules() {
  return this._contentScriptRules;
}

ChromeExtension.prototype.matchesPermissions = function matchesPermissions(aUrl) {
  for (var i = 0, l = this._matchesPermissions.length; i < l; i++) {
    if (this._matchesPermissions[i].matches(aUrl)) {
      return true;
    }
  }
  return false;
}

ChromeExtension.prototype.getFileContent = function getFileContent(aFile) {
  return FILE.readFile(FILE.getFile(this._extensionDirectory, aFile));
}

//TODO
ChromeExtension.prototype.onNotification = function onNotification(aServiceName, aEvent, aArgs) {
  switch (aServiceName) {
    case WINDOWS.SERVICENAME :
      switch (aEvent) {
        case WINDOWS.ON_NEW_WINDOW_EVENT :
          this.loadBrowserAction.apply(this, aArgs);
          break;
        default :
          if (this._windows != null) {
            this._windows.onNotification(aEvent, aArgs);
          }
      }
      break;
    case TABS.SERVICENAME :
      if (this._tabs != null) {
        this._tabs.onNotification(aEvent, aArgs);
      }
      break;
  }
}

