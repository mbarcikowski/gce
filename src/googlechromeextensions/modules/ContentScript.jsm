Components.utils.import("resource://googlechromeextensions/services/CONSOLE.jsm");
Components.utils.import("resource://googlechromeextensions/View.jsm");
Components.utils.import("resource://googlechromeextensions/ChromeEvent.jsm");

var EXPORTED_SYMBOLS = ["ContentScript"];


function ContentScript() {
	View.call(this);
  this._onRequest = null;
  this._onConnect = null;
  this._sandbox = null;
}

ContentScript.prototype = new View;

ContentScript.prototype.init = function init(aGoogleChromeEngine, aChromeWindow, aTab) {
	View.prototype.init.call(this, aGoogleChromeEngine, aChromeWindow, null);
  this._tab = aTab;
  this._onRequest = new ChromeEvent().init();
  this._onConnect = new ChromeEvent().init();
  this._createSandbox(aChromeWindow);
  return this;
}

ContentScript.prototype.getOnRequest = function getOnRequest() {
  return this._onRequest;
}

ContentScript.prototype.getOnConnect = function getOnConnect() {
  return this._onConnect;
}

ContentScript.prototype.executeScript = function executeScript(aScript) {
  Components.utils.evalInSandbox(aScript, this._sandbox);
}

ContentScript.prototype._createSandbox = function _createSandbox(aChromeWindow) {
  var DOMWindow = aChromeWindow.QueryInterface(Components.interfaces.nsIDOMWindow);
  var sandbox = Components.utils.Sandbox(DOMWindow);
  sandbox.console = CONSOLE.getWrapper();
  //TODO get document wrapper
  sandbox.document = DOMWindow.wrappedJSObject.document;
  //TODO get window wrapper
  sandbox.window = DOMWindow;
  sandbox.chrome = {
    extension : this._googleChromeEngine.getExtension().initContentScriptWrapper(this),
    windows : undefined,
    bookmarks : undefined,
    browserAction : undefined,
    i18n : undefined,
    tabs : undefined
  };
  this._sandbox = sandbox;
}

ContentScript.prototype.finalize = function(){
	this._googleChromeEngine.getExtension().releaseContentScriptWrapper(this);
  this._onRequest.releaseWrapper(this);
  this._onConnect.releaseWrapper(this);
}
