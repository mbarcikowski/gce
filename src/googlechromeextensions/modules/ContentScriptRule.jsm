var EXPORTED_SYMBOLS = ["ContentScriptRule"];

function ContentScriptRule() {
  this._runAt = null;
  this._allFrames = null;
  this._matchers = new Array();
  this._css = new Array();
  this._js = new Array();
}

ContentScriptRule.prototype._DOCUMENT_IDLE = "document_idle";
ContentScriptRule.prototype._DOCUMENT_START = "document_start";
ContentScriptRule.prototype._DOCUMENT_END = "document_end";

ContentScriptRule.prototype.init = function init() {
  return this;
}

ContentScriptRule.prototype.setAllFrames = function setAllFrames(aAllFrames) {
  this._allFrames = aAllFrames;
}

ContentScriptRule.prototype.getAllFrames = function getAllFrames() {
  return this._allFrames;
}

ContentScriptRule.prototype.setRunAt = function setRunAt(aRunAt) {
  this._runAt = aRunAt;
}

ContentScriptRule.prototype.isRunAtIdle = function isRunAtIdle() {
  return this._runAt == this._DOCUMENT_IDLE;
}

ContentScriptRule.prototype.isRunAtStart = function isRunAtStart() {
  return this._runAt == this._DOCUMENT_START;
}

ContentScriptRule.prototype.isRunAtEnd = function isRunAtEnd() {
  return this._runAt == this._DOCUMENT_END;
}

ContentScriptRule.prototype.addMatch = function addMatch(aMatch) {
  this._matchers.push(aMatch);
}

ContentScriptRule.prototype.addJS = function addJS(aJS) {
  this._js.push(aJS);
}

ContentScriptRule.prototype.addCSS = function addCSS(aCss) {
  this._css.push(aCss);
}

ContentScriptRule.prototype.matches = function matches(aUrl) {
  for (var i = 0, l = this._matchers.length; i < l; i++) {
    if (this._matchers[i].matches(aUrl)) {
      return true;
    }
  }
  return false;
}

ContentScriptRule.prototype.getCSS = function getCSS() {
  return this._css;
}

ContentScriptRule.prototype.getJS = function getJS() {
  return this._js;
}