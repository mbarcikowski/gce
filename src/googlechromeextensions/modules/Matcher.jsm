var EXPORTED_SYMBOLS = ["Matcher"];

function Matcher() {
  this._pattern = null;
}

Matcher.prototype._matchPattern = "^(http|https|file|ftp)://(?:(\\*)|(?:(\\*\\.)([^/*]+))|([^/*]+))/(.*)$";

Matcher.prototype.init = function init(aMatchString) {
  var matchRegExp = new RegExp(this._matchPattern, "g");
  var groups = null;
  if (aMatchString == null || ((groups = matchRegExp.exec(aMatchString + "")) == null)) {
    throw new Error("Invalid match");
  }

  var pattern = "^" + groups[1] + "://";
  if (groups[2]) {
    pattern += "[^/]+";
  }
  else if (groups[3]) {
    pattern += "[^/]+\\." + this.escapePattern(groups[4]);
  }
  else {
    pattern += this.escapePattern(groups[5]);
  }
  pattern += "/" + this.escapePattern(groups[6]);

  this._pattern = pattern;
  return this;
}

Matcher.prototype.matches = function matches(aUrl) {
  var matchRegExp = new RegExp(this._pattern, "g");
  return matchRegExp.test(aUrl);
}

Matcher.prototype.escapePattern = function escapePattern(aPattern) {
  var re = /[.\\*]/g;
  return aPattern.replace(re, this.replacechar);
}

Matcher.prototype.replacechar = function (match) {
  if (match == ".") {
    return "\\.";
  }
  else if (match == "\\") {
    return "\\\\";
  }
  else if (match == "*") {
      return ".*";
    }
}