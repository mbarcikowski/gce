var EXPORTED_SYMBOLS = ["Version"];

function Version() {
	this._versionNumbers = new Array();
	this._versionString = null;
}

Version.prototype._versionPattern = "^\\d+(\\.\\d+){0,3}$";
Version.prototype._errorMessage = "It must be between 1-4 dot-separated integers.";

Version.prototype.init = function init(aVersionString) {
	this._versionString = aVersionString;
	var versionRegExp = new RegExp(this._versionPattern, "g");
	if (aVersionString == null || !versionRegExp.test(aVersionString + "")) {
		throw new Error(this._errorMessage);
	}
	aVersionString = aVersionString + "";
	var splits = aVersionString.split(".");
	for (var i = 0, l = splits.length; i < l; i++) {
		var number;
		var split = splits[i];
		if (split.length == 1) {
			try {
				number = new Number(split);
			}
			catch(e) {
				throw new Error(this._errorMessage);
			}
		}
		else {
			if (split[0] == "0") {
				throw new Error(this._errorMessage);
			}
			try {
				number = new Number(split);
			}
			catch(e) {
				throw new Error(this._errorMessage);
			}
			if (number < 0 || number > 65535) {
				throw new Error(this._errorMessage);
			}
		}
		this._versionNumbers.push(number);
	}
	return this;
}

Version.prototype.getVersionString = function getVersionString() {
	return this._versionString;
}
