var EXPORTED_SYMBOLS = ["View"];

function View() {
	this._googleChromeEngine = null;
  this._chromeWindow = null;
  this._id = null;
	this._tab = null;
  this._ports = new Array(); //TODO ports  
}

View.prototype.init = function init(aGoogleChromeEngine, aChromeWindow, aID) {
	this._googleChromeEngine = aGoogleChromeEngine;
  this._chromeWindow = aChromeWindow;
  this._id = aID;
  return this;
}

View.prototype.getID = function getID(){
  return this._id;
}

View.prototype.getChromeWindow = function getChromeWindow(){
  return this._chromeWindow; 
}

View.prototype.getGoogleChromeEngine = function getGoogleChromeEngine(){
  return this._googleChromeEngine;
}

View.prototype.setTab = function setTab(aTab){
	this._tab = aTab;
}

View.prototype.getTab = function getTab(aTab){
	return this._tab;	
}

View.prototype.getLocation = function getLocation(){
	return this._chromeWindow.location.pathname.substring(1);
}

View.prototype.finalize = function finalize(){
	this._googleChromeEngine.releaseChromeWrapper(this);
  for (var i = 0, l = this._ports.length; i < l; i++) {
    var port = this._ports[i];
    port.getOnDisconnect().callbackNotify(null,
        function(aPort) {
          return function(aListenerView) {
            return [aPort.initWrapper(aListenerView, false)];
          }
        }(port)
        );
    port.finalize();
  }
  this._ports = new Array();
}

View.prototype.removePort = function removePort(aPort) {
  var ports = new Array();
  for (var i = 0, l = this._ports.length; i < l; i++) {
    var port = this._ports[i];
    if (port != aPort) {
      ports.push(port);
    }
  }
  this._ports = ports;
}

View.prototype.releaseWrapper = function releaseWrapper(aView) {
  var ports = new Array();
  for (var i = 0, l = this._ports.length; i < l; i++) {
    var port = this._ports[i];
    if (port.getView() == aView) {
      port.getOnDisconnect().callbackNotify(aView,
          function(aPort) {
            return function(aListenerView) {
              return [aPort.initWrapper(aListenerView, false)];
            }
          }(port)
          );
      port.finalize();
    }
    else {
      if (!port.releaseWrapper(aView)) {
        ports.push(port);
      }
    }
  }
  this._ports = ports;
}



