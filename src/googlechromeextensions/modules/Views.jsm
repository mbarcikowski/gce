Components.utils.import("resource://googlechromeextensions/View.jsm");

var EXPORTED_SYMBOLS = ["Views"];

function Views() {
  this._googleChromeEngine = null;
  this._currentViewId = -1;
  this._viewsMap = {};
}

Views.prototype.init = function init(aGoogleChromeEngine) {
  this._googleChromeEngine = aGoogleChromeEngine;
  return this;
}

Views.prototype._getNewViewID = function _getNewViewID() {
  return ++this._currentViewId;
}

Views.prototype.addView = function addView(aChromeWindow) {
  var view = new View().init(this._googleChromeEngine, aChromeWindow, this._getNewViewID());
  this._viewsMap[view.getID()] = view;
  return view;
}

Views.prototype.removeView = function removeView(aView) {
  var id = aView.getID();
  var view = this._viewsMap[id];
  if (view != null) {
    delete (this._viewsMap[id]);
    view.finalize();
  }
}

Views.prototype.getViewsAsArray = function getViewsAsArray() {
  var views = new Array();
  for (var id in this._viewsMap) {
    views.push(this._viewsMap[id]);
  }
  return views;
}