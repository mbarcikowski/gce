Components.utils.import("resource://googlechromeextensions/services/CONSOLE.jsm");
Components.utils.import("resource://googlechromeextensions/ChromeEvent.jsm");

var EXPORTED_SYMBOLS = ["BrowserAction"];

function BrowserAction() {
	this._googleChromeEngine = null;
	this._active = false;
	this._icon = null;
	this._title = null;
	this._popup = null;
	this._badgeText = null;
	this._badgeBackgroundColor = null;
	this._onClicked = null;
	this._browserActionElements = new Array();
	this._view = null;
}

BrowserAction.prototype._webProgressListenerIID = Components.interfaces.nsIWebProgressListener;
BrowserAction.prototype._webProgressIID = Components.interfaces.nsIWebProgress;

BrowserAction.prototype.init = function init(aGoogleChromeEngine) {
	this._googleChromeEngine = aGoogleChromeEngine;
	this._onClicked = new ChromeEvent().init();
	return this;
}

BrowserAction.prototype.isActive = function isActive() {
	return this._active;
}

BrowserAction.prototype.setActive = function setActive(aIsActive) {
	this._active = aIsActive;
}

BrowserAction.prototype.getIcon = function getIcon() {
	return this._icon;
}

BrowserAction.prototype.setIconImageData = function setIconImageData(aImageData) {
	this._icon = aImageData;
	for (var i = 0, l = this._browserActionElements.length; i < l; i++) {
		try {
			this._browserActionElements[i].setAttribute("icon", this._icon);
		}
		catch(e) {
			CONSOLE.error(e);
		}
	}
}
BrowserAction.prototype.setIconPath = function setIconPath(aIconPath) {
	this._icon = this._googleChromeEngine.convertToExtensionUrl(aIconPath);
	for (var i = 0, l = this._browserActionElements.length; i < l; i++) {
		try {
			this._browserActionElements[i].setAttribute("icon", this._icon);
		}
		catch(e) {
			CONSOLE.error(e);
		}
	}
}

BrowserAction.prototype.getTitle = function getTitle() {
	return this._title;
}

BrowserAction.prototype.setTitle = function setTitle(aTitle) {
	this._title = aTitle;
	for (var i = 0, l = this._browserActionElements.length; i < l; i++) {
		this._browserActionElements[i].setAttribute("title", aTitle);
	}
}

BrowserAction.prototype.setView = function setView(aView) {
	this._view = aView;
}

BrowserAction.prototype.getView = function getView() {
	return this._view;
}

BrowserAction.prototype.setBadgeText = function setBadgeText(aText) {
	this._badgeText = aText;
	var hide = this._badgeText.length == 0;
	for (var i = 0, l = this._browserActionElements.length; i < l; i++) {
		try {
			var browseractionElement = this._browserActionElements[i];
			if (hide) {
				browseractionElement.setAttribute("badgetexthidden", true);
			}
			else {
				browseractionElement.setAttribute("badgetexthidden", false);
			}
			browseractionElement.setAttribute("badgetext", aText);
		}
		catch(e) {
			CONSOLE.error(e);
		}
	}
}

BrowserAction.prototype.getBadgeText = function getBadgeText() {
	return this._badgeText;
}

BrowserAction.prototype.setBadgeBackgroundColor = function getBadgeBackgroundColor(aColor) {
	this._badgeBackgroundColor = aColor;
	for (var i = 0, l = this._browserActionElements.length; i < l; i++) {
		try {
			var browseractionElement = this._browserActionElements[i];
			var style = "";
			style += "background-color : rgb(" + aColor[0] + "," + aColor[1] + "," + aColor[2] + ");";
			style += "opacity : " + (aColor[3] / 255) + "";
			browseractionElement.setAttribute("badgestyle", style);
		}
		catch(e) {
			CONSOLE.error(e);
		}
	}
}

BrowserAction.prototype.getBadgeBackgroundColor = function getBadgeBackgroundColor() {
	return this._badgeBackgroundColor;
}


BrowserAction.prototype.getPopup = function getPopup() {
	return this._popup;
}

BrowserAction.prototype.setPopup = function setPopup(aPopup) {
	this._popup = aPopup;
}

BrowserAction.prototype.getOnClicked = function getOnClicked() {
	return this._onClicked;
}

BrowserAction.prototype.getBrowserActionElements = function getBrowserActionElements() {
	return this._browserActionElements;
}

BrowserAction.prototype.createBrowserActionElement = function createBrowserActionElement(aWindow) {
	try {
		var mainWindow = aWindow;
		var doc = mainWindow.document;
		var browseractionElement = doc.createElementNS("http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul", "browseraction");
		browseractionElement.setAttribute("class", "browserAction");
		browseractionElement.setAttribute("icon", this._icon);
		if (this._title != null) {
			browseractionElement.setAttribute("title", this._title);
		}
		if (this._badgeText != null) {
			if (this._badgeText.length == 0) {
				browseractionElement.setAttribute("badgetexthidden", true);
			}
			else {
				browseractionElement.setAttribute("badgetexthidden", false);
			}
			browseractionElement.setAttribute("badgetext", this._badgeText);
		}
		else {
			browseractionElement.setAttribute("badgetexthidden", true);
		}

		var style = "";
		if (this._badgeBackgroundColor != null) {
			style += "background-color : rgb(" + this._badgeBackgroundColor[0] + "," + this._badgeBackgroundColor[1] + "," + this._badgeBackgroundColor[2] + ");";
			style += "opacity : " + (this._badgeBackgroundColor[3] / 255) + "";
		}
		else {
			style += "background-color : rgb(255, 0, 0);";
			style += "opacity : 1";
		}
		browseractionElement.setAttribute("badgestyle", style);
		browseractionElement.addEventListener("click", function(aBrowserAction, aBrowseractionElement) {
			return function(e) {
				if (aBrowserAction._popup != null && e.button == 0) {

					var popup = aBrowseractionElement.ownerDocument.getAnonymousNodes(aBrowseractionElement)[1];
					if (popup.state == "closed") {
						var anchor = aBrowseractionElement.ownerDocument.getAnonymousNodes(aBrowseractionElement)[0];
						popup.openPopup(anchor, "after_end", 0, -3, false, false);
					}
				}
				else {
					var tab = null;
					var listeners = aBrowserAction._onClicked.notify(null, [tab]);
				}
			}
		}(this, browseractionElement), true);

		browseractionElement.addEventListener("popupshowing", function(aBrowserAction, aBrowseractionElement) {
			return function(e) {
				var iframe = e.originalTarget.querySelector(".browseraction-popup-iframe");
				var webNavigation = iframe.contentWindow.QueryInterface(Components.interfaces.nsIInterfaceRequestor).getInterface(Components.interfaces.nsIWebNavigation);
				var docShell = webNavigation.QueryInterface(Components.interfaces.nsIDocShell);
				var req = docShell.QueryInterface(Components.interfaces.nsIInterfaceRequestor);
				var prog = req.getInterface(Components.interfaces.nsIWebProgress);
				aBrowserAction.addProgressListener(prog);
				aBrowseractionElement.setAttribute("checked", true);
				iframe.focus();
				iframe.contentDocument.location.href = aBrowserAction._popup;
			}
		}(this, browseractionElement), true);

		browseractionElement.addEventListener("popupshown", function(aBrowserAction, aBrowseractionElement) {
			return function(e) {
				var popup = e.originalTarget;
				var iframe = e.originalTarget.querySelector(".browseraction-popup-iframe");
				var border = e.originalTarget.querySelector(".browseraction-popup-border");
				var width = Math.max(10, Math.min(iframe.contentDocument.documentElement.scrollWidth, 800));
				var height = Math.max(10, Math.min(iframe.contentDocument.documentElement.scrollHeight, 600));
				iframe.style.width = (width) + "px";
				iframe.style.height = (height) + "px";
				popup.style.width = (border.scrollWidth) + "px";
				popup.style.height = (border.scrollHeight + 5 ) + "px";

				iframe.contentDocument.documentElement.addEventListener("DOMSubtreeModified", function(e) {
					var width = Math.min(iframe.contentDocument.body.scrollWidth, 800);
					var height = Math.min(iframe.contentDocument.body.scrollHeight, 600);
					iframe.style.width = (width) + "px";
					iframe.style.height = (height) + "px";
					popup.style.width = (border.scrollWidth) + "px";
					popup.style.height = (border.scrollHeight + 5 ) + "px";
				}, false)

			}
		}(this, browseractionElement), true);

		browseractionElement.addEventListener("popuphiding", function(aBrowserAction, aBrowseractionElement) {
			return function(e) {
				var iframe = e.originalTarget.querySelector(".browseraction-popup-iframe");
				var webNavigation = iframe.contentWindow.QueryInterface(Components.interfaces.nsIInterfaceRequestor).getInterface(Components.interfaces.nsIWebNavigation);
				var docShell = webNavigation.QueryInterface(Components.interfaces.nsIDocShell);
				var req = docShell.QueryInterface(Components.interfaces.nsIInterfaceRequestor);
				var prog = req.getInterface(Components.interfaces.nsIWebProgress);
				aBrowserAction.removeProgressListener(prog);
				aBrowseractionElement.removeAttribute("checked");
				iframe.contentDocument.location.href = "about:blank";
				iframe.style.width = null;
				iframe.style.height = null;
			}
		}(this, browseractionElement), true);

		this._browserActionElements.push(browseractionElement);
		doc.getElementById("nav-bar").insertBefore(browseractionElement, doc.getElementById("urlbar-container").nextSibling);
	} catch(e) {
		CONSOLE.error(e);
	}
}

BrowserAction.prototype.getDataUrl = function getDataUrl(aImageData) {
	return	this._googleChromeEngine.getDataUrl(aImageData);
}

BrowserAction.prototype.onLocationChange = function onLocationChange(webProgress, request, location) {

}

BrowserAction.prototype.onProgressChange = function onProgressChange(webProgress, request, curSelfProgress) {

}

BrowserAction.prototype.onSecurityChange = function onSecurityChange(webProgress, request, state) {

}

BrowserAction.prototype.onStatusChange = function onStateChange(webProgress, request, aStatus, aMessage) {

}

BrowserAction.prototype.onStateChange = function onStateChange(webProgress, request, stateFlags, status) {
	var state = this._webProgressListenerIID.STATE_IS_REQUEST | this._webProgressListenerIID.STATE_IS_BROKEN | this._webProgressListenerIID.STATE_SECURE_MED;
	var chromeWindow = webProgress.DOMWindow;
	if ((state == stateFlags) &&
			chromeWindow != null &&
			chromeWindow.location.href.indexOf(this._googleChromeEngine.getExtensionUrl()) == 0) {
		this.bindJSContext(chromeWindow);
	}
}


BrowserAction.prototype.QueryInterface = function QueryInterface(aIID) {
	if (aIID.equals(this._webProgressListenerIID) ||
			aIID.equals(Components.interfaces.nsISupportsWeakReference) ||
			aIID.equals(Components.interfaces.nsIObserver) ||
			aIID.equals(Components.interfaces.nsISupports))
		return this;
	throw Components.results.NS_NOINTERFACE;
}

BrowserAction.prototype.addProgressListener = function addProgressListener(prog) {
	var view = this._view;
	if (view != null) {
		this._googleChromeEngine.getViews().removeView(this._view);
		this._view = null;
	}
	prog.addProgressListener(this, this._webProgressIID.NOTIFY_ALL);
}

BrowserAction.prototype.removeProgressListener = function removeProgressListener(prog) {
	var view = this._view;
	if (view != null) {
		this._googleChromeEngine.getViews().removeView(this._view);
		this._view = null;
	}
	prog.removeProgressListener(this, this._webProgressIID.NOTIFY_ALL);
}

BrowserAction.prototype.bindJSContext = function bindJSContext(aChromeWindow) {
	var view = this._view;
	if (view == null) {
		view = this._googleChromeEngine.getViews().addView(aChromeWindow);
		this._view = view;
		var chrome = this._googleChromeEngine.initChromeWrapper(this._view);
		aChromeWindow.chrome = chrome;
		var console = CONSOLE.getWrapper();
		aChromeWindow.console = console;
	}
}

BrowserAction.prototype.initWrapper = function initWrapper(aView) {
	var wrapper = {
		setTitle : function(aBrowserAction) {
			return function setTitle(aDetails) {
				if (aDetails != null && aDetails.title != null) {
					if (aDetails.tabId != null) {
						//TODO Not implemented
						throw new Error("Not implemented");
					}
					else {
						aBrowserAction.setTitle(aDetails.title);
					}
				}
				else {
					//TODO Throw error ?
					CONSOLE.error("bad details");
				}
			}
		}(this),

		setBadgeBackgroundColor : function(aBrowserAction) {
			return function setBadgeBackgroundColor(aDetails) {
				if (aDetails != null && aDetails.color != null && aDetails.color.length == 4) {
					if (aDetails.tabId != null) {
						//TODO Not implemented
						throw new Error("Not implemented");
					}
					else {
						aBrowserAction.setBadgeBackgroundColor(aDetails.color);
					}
				}
				else {
					//TODO Throw error ?
					CONSOLE.error("bad details");
				}
			}
		}(this),

		setBadgeText : function(aBrowserAction) {
			return function setBadgeText(aDetails) {
				if (aDetails != null && aDetails.text != null) {
					if (aDetails.tabId != null) {
						//TODO Not implemented
						throw new Error("Not implemented");
					}
					else {
						aBrowserAction.setBadgeText(aDetails.text);
					}
				}
				else {
					//TODO Throw error ?
					CONSOLE.error("bad details");
				}
			}
		}(this),

		setIcon :	function(aBrowserAction) {
			return function setIcon(aDetails) {
				if (aDetails != null && (aDetails.imageData != null || aDetails.path != null )) {
					if (aDetails.tabId != null) {
						//TODO Not implemented
						throw new Error("Not implemented");
					}
					else {
						if (aDetails.imageData != null) {
							aBrowserAction.setIconImageData(aBrowserAction.getDataUrl(aDetails.imageData));
						}
						else {
							aBrowserAction.setIconPath(aDetails.path);
						}
					}

				} else {
					//TODO Throw error ?
					CONSOLE.error("bad details");
				}
			}
		}(this),


		onClicked : function (aBrowserAction) {
			return aBrowserAction.getOnClicked().initWrapper(aView);
		}(this, aView)
	};
	return wrapper;
}

BrowserAction.prototype.releaseWrapper = function releaseWrapper(aView) {
	this._onClicked.releaseWrapper(aView);
}