Components.utils.import("resource://googlechromeextensions/services/CONSOLE.jsm");

var EXPORTED_SYMBOLS = ["BackgroundPage"];

function BackgroundPage() {
    this._googleChromeEngine = null;
    this._backgroundPage = null;
    this._view = null;
}

BackgroundPage.prototype._webProgressListenerIID = Components.interfaces.nsIWebProgressListener;
BackgroundPage.prototype._webProgressIID = Components.interfaces.nsIWebProgress;

BackgroundPage.prototype.init = function init(aGoogleChromeEngine) {
    this._googleChromeEngine = aGoogleChromeEngine;
    return this;
}

BackgroundPage.prototype.setView = function setView(aView) {
    this._view = aView;
}

BackgroundPage.prototype.getView = function getView() {
    return this._view;
}

BackgroundPage.prototype.setBackgroundPage = function setBackgroundPage(aBackgroundPage) {
    this._backgroundPage = aBackgroundPage;
}

BackgroundPage.prototype.getBackgroundPage = function getBackgroundPage() {
    return this._backgroundPage;
}

BackgroundPage.prototype.onLocationChange = function onLocationChange(webProgress, request, location) {

}

BackgroundPage.prototype.onProgressChange = function onProgressChange(webProgress, request, curSelfProgress) {

}

BackgroundPage.prototype.onSecurityChange = function onSecurityChange(webProgress, request, state) {

}

BackgroundPage.prototype.onStatusChange = function onStateChange(webProgress, request, aStatus, aMessage) {

}

BackgroundPage.prototype.onStateChange = function onStateChange(webProgress, request, stateFlags, status) {
    var state = this._webProgressListenerIID.STATE_IS_REQUEST | this._webProgressListenerIID.STATE_IS_BROKEN | this._webProgressListenerIID.STATE_SECURE_MED;
    var chromeWindow = webProgress.DOMWindow;
    if ((state == stateFlags) &&
        chromeWindow != null &&
        chromeWindow.location.href.indexOf(this._googleChromeEngine.getExtensionUrl()) == 0) {
        this.bindJSContext(chromeWindow);
    }
}


BackgroundPage.prototype.QueryInterface = function QueryInterface(aIID) {
    if (aIID.equals(this._webProgressListenerIID) ||
        aIID.equals(Components.interfaces.nsISupportsWeakReference) ||
        aIID.equals(Components.interfaces.nsIObserver) ||
        aIID.equals(Components.interfaces.nsISupports))
        return this;
    throw Components.results.NS_NOINTERFACE;
}

BackgroundPage.prototype.addProgressListener = function addProgressListener(aWebProgress) {
    aWebProgress.addProgressListener(this, this._webProgressIID.NOTIFY_ALL);
}

BackgroundPage.prototype.removeProgressListener = function removeProgressListener(aWebProgress) {
    aWebProgress.removeProgressListener(this, this._webProgressIID.NOTIFY_ALL);
}

BackgroundPage.prototype.bindJSContext = function bindJSContext(aChromeWindow) {
    var view = this._view;
    if (view == null) {
        view = this._googleChromeEngine.getViews().addView(aChromeWindow);
        this._view = view;    
        var window = new XPCSafeJSObjectWrapper(aChromeWindow);
        var chrome = this._googleChromeEngine.initChromeWrapper(this._view);
        window['chrome'] = chrome;
        window['console'] =  CONSOLE.getWrapper();
    }
}

//TODO finalize
BackgroundPage.prototype.finalize = function finalize(){

}