Components.utils.import("resource://googlechromeextensions/services/WINDOWS.jsm");
Components.utils.import("resource://googlechromeextensions/ChromeEvent.jsm");
Components.utils.import("resource://googlechromeextensions/api/extension/Port.jsm");
Components.utils.import("resource://googlechromeextensions/api/extension/MessageSender.jsm");

var EXPORTED_SYMBOLS = ["Extension"];

function Extension() {
  this._googleChromeEngine = null;
  this._onConnect = null;
  this._onConnectExternal = null;
  this._onRequest = null;
  this._onRequestExternal = null;
  this._ports = new Array();
}

Extension.prototype.init = function init(aGoogleChromeEngine) {
  this._googleChromeEngine = aGoogleChromeEngine;
  this._onConnect = new ChromeEvent().init();
  this._onConnectExternal = new ChromeEvent().init();
  this._onRequest = new ChromeEvent().init();
  this._onRequestExternal = new ChromeEvent().init();
  return this;
}

Extension.prototype.getOnConnect = function getOnConnect() {
  return this._onConnect;
}

Extension.prototype.getOnConnectExternal = function getOnConnectExternal() {
  return this._onConnectExternal;
}

Extension.prototype.getOnRequest = function getOnRequest() {
  return this._onRequest;
}

Extension.prototype.getOnRequestExternal = function getOnRequestExternal() {
  return this._onRequestExternal;
}

Extension.prototype._connect = function _connect(aView, aTab, aExtensionId, aName) {
  if (false && aExtensionId != null) {
    //TODO take into account aExtensionId
    throw new Error("not implemented");
  }
  else {
    var messageSender = new MessageSender().init(this._googleChromeEngine.getID(), aTab);
    var port = new Port().init(this._googleChromeEngine, aView, aName, messageSender);
    this._ports.push(port);
    if (!this._onConnect.callbackNotify(aView,
        function(aPort) {
          return function(aListenerView) {
            return [port.initWrapper(aListenerView, true)];
          }
        }(port)
        ))
    {
      aView.getChromeWindow().setTimeout(function(aPort) {
        return function() {
          aPort.releaseWrapper(null);
        }
      }(port), 0);
    }
    return port.initWrapper(aView, false);
  }
}

Extension.prototype._sendRequest = function _sendRequest(aView, aTab, aExtensionId, aRequest, aResponseCallback) {
  if (false && aExtensionId != null) {
    //TODO take into account aExtensionId
    throw new Error("not implemented");
  }
  else {
    var messageSender = new MessageSender().init(this._googleChromeEngine.getID(), aTab);
    var port = new Port().init(this._googleChromeEngine, aView, null, messageSender);
    var portSource = port.initWrapper(aView, false);
    if (aResponseCallback != null) {
      portSource.onMessage.addListener(aResponseCallback);
    }
    this._onRequest.callbackNotify(aView,
        function(aPort, aInnerRequest) {
          return function(aListenerView) {
            var portTarget = port.initWrapper(aListenerView, true);
            return [aInnerRequest,
              portTarget.sender, function(aResponse) {
                portTarget.postMessage(aResponse);
              }];
          }
        }(port, aRequest)
        );
    port.finalize();
  }
}

Extension.prototype._getViews = function _getViews() {
  var views = new Array();
  var viewsArray = this._googleChromeEngine.getViews().getViewsAsArray();
  for (var i = 0; i < viewsArray.length; i++) {
    views.push(viewsArray[i].getChromeWindow());
  }
  return views;
}

Extension.prototype._getExtensionTabs = function _getExtensionTabs(aWindowId){
  var selectedWindow = null;
  if (aWindowId != null) {
    selectedWindow = this._googleChromeEngine.getWindows().getWindow(aWindowId);
  }
  //TODO error if not selectedWindow ?
  if (selectedWindow == null) {
    selectedWindow = this._googleChromeEngine.getWindows().getCurrentWindow();
  }
  /*
   if (aWindowId != null) {
    selectedWindow = WINDOWS.getWindow(aWindowId);
  }
  //TODO error if not selectedWindow ?
  if (selectedWindow == null) {
    selectedWindow = WINDOWS.getCurrentWindow();
  }
   */
  var tabs = this._googleChromeEngine.getTabs();
  var tabsViews = new Array();
  var gBrowser = selectedWindow.getChromeWindow().gBrowser;
  if (gBrowser != null) {
    var tabContainer = gBrowser.tabContainer;
    var currentSibling = tabContainer.firstChild;
    while (currentSibling != null) {
      var tab = tabs._findTab(currentSibling);
      if (tab != null && tab.getView() != null) {
        tabsViews.push(tab.getView().getChromeWindow());
      }
      currentSibling = currentSibling.nextSibling;
    }
  }
  return tabsViews;
}

Extension.prototype.initContentScriptWrapper = function initContentScriptWrapper(aContentScript) {
  var wrapper = {
    lastError : undefined,
    onRequest :  function (aInnerContentScript) {
      return aInnerContentScript.getOnRequest().initWrapper(aInnerContentScript);
    }(aContentScript),
    onConnect :  function (aInnerContentScript) {
      return aInnerContentScript.getOnConnect().initWrapper(aInnerContentScript);
    }(aContentScript),
    connect : function(aExtension, aInnerContentScript) {
      return function(aExtensionId, aConnectInfo) {
        var name = null;
        if (aConnectInfo != null) {
          name = aConnectInfo.name;
        }
        return aExtension._connect(aInnerContentScript, aInnerContentScript.getTab(), aExtensionId, name);
      }
    }(this, aContentScript),
    sendRequest: function(aExtension, aInnerContentScript) {
      return function(aExtensionId, aRequest, aResponseCallback) {
        if (aRequest == null) {
          //TODO throw error?
        }
        aExtension._sendRequest(aInnerContentScript, aInnerContentScript.getTab(), aExtensionId, aRequest, aResponseCallback);
      }
    }(this, aContentScript),
    getURL :  function (aExtension) {
      return function getURL(aPath) {
        return aExtension._googleChromeEngine.convertToExtensionUrl(aPath);
      }
    }(this),

    getBackgroundPage : function() {
      throw new Error("Error: chrome.extension.getBackgroundPage is not supported in content scripts. See the content scripts documentation for more details.");
    },
    getExtensionTabs : function() {
      throw new Error("Error: chrome.extension.getExtensionTabs is not supported in content scripts. See the content scripts documentation for more details.");
    },
    getViews : function() {
      throw new Error("Error: chrome.extension.getViews is not supported in content scripts. See the content scripts documentation for more details.");
    },
    onConnectExternal : function() {
      throw new Error("Error: chrome.extension.onConnectExternal is not supported in content scripts. See the content scripts documentation for more details.");
    },
    onRequestExternal : function() {
      throw new Error("Error: chrome.extension.onRequestExternal is not supported in content scripts. See the content scripts documentation for more details.");
    }
  }
  return wrapper;
}

Extension.prototype.releaseContentScriptWrapper = function releaseContentScriptWrapper(aContentScript) {
  var ports = new Array();
  for (var i = 0, l = this._ports.length; i < l; i++) {
    var port = this._ports[i];
    if (port.getView() == aContentScript) {
      port.getOnDisconnect().callbackNotify(aContentScript,
          function(aPort) {
            return function(aListenerView) {
              return [aPort.initWrapper(aListenerView, false)];
            }
          }(port)
          );
      port.finalize();
    }
    else {
      if (!port.releaseWrapper(aContentScript)) {
        ports.push(port);
      }
    }
  }
  this._ports = ports;
}

//TODO lastError
Extension.prototype.initWrapper = function initWrapper(aView) {
  var wrapper = {
    lastError : undefined,

    connect : function(aExtension, aInnerView) {
      return function(aExtensionId, aConnectInfo) {
        var name = null;
        if (aConnectInfo != null) {
          name = aConnectInfo.name;
        }
        return aExtension._connect(aInnerView, aInnerView.getTab(), aExtensionId, name);
      }
    }(this, aView),

    getBackgroundPage : function(aExtension) {
      return function getBackgroundPage() {
        var view = aExtension._googleChromeEngine.getBackgroundPage().getView();
        if (view != null) {
          return view.getChromeWindow();
        }
        else {
          return null;
        }
      }
    }(this),

    getExtensionTabs : function(aExtension) {
      return function getExtensionTabs(aWindowId) {
        return aExtension._getExtensionTabs(aWindowId);
      }
    }(this),

    getURL :  function(aExtension) {
      return function getURL(aPath) {
        return aExtension._googleChromeEngine.convertToExtensionUrl(aPath);
      }
    }(this),

    getViews : function(aExtension) {
      return function getViews() {
        return aExtension._getViews();
      }
    }(this),

    sendRequest: function(aExtension, aInnerView) {
      return function(aExtensionId, aRequest, aResponseCallback) {
        if (aRequest == null) {
          //TODO throw error?
        }
        aExtension._sendRequest(aInnerView, aInnerView.getTab(), aExtensionId, aRequest, aResponseCallback);
      }
    }(this, aView),

    onConnect : function(aExtension, aInnerView) {
      return aExtension.getOnConnect().initWrapper(aView);
    }(this, aView),

    //TODO Not implemented
    onConnectExternal : function(aExtension, aInnerView) {
      return aExtension.getOnConnectExternal().initWrapper(aView);
    }(this, aView),

    onRequest : function(aExtension, aInnerView) {
      return aExtension.getOnRequest().initWrapper(aView);
    }(this, aView),

    //TODO Not implemented
    onRequestExternal : function(aExtension, aInnerView) {
      return aExtension.getOnRequestExternal().initWrapper(aView);
    }(this, aView)
  }
  return wrapper;
}

Extension.prototype.releaseWrapper = function releaseWrapper(aView) {
  var ports = new Array();
  for (var i = 0, l = this._ports.length; i < l; i++) {
    var port = this._ports[i];
    if (port.getView() == aView) {
      port.getOnDisconnect().callbackNotify(aView,
          function(aPort) {
            return function(aListenerView) {
              return [aPort.initWrapper(aListenerView, true)];
            }
          }(port)
          );
      port.finalize();
    }
    else {
      if (!port.releaseWrapper(aView)) {
        ports.push(port);
      }
    }
  }
  this._ports = ports;
  this._onConnect.releaseWrapper(aView);
  this._onConnectExternal.releaseWrapper(aView);
  this._onRequest.releaseWrapper(aView);
  this._onRequestExternal.releaseWrapper(aView);
}

Extension.prototype.removePort = function removePort(aPort) {
  var ports = new Array();
  for (var i = 0, l = this._ports.length; i < l; i++) {
    var port = this._ports[i];
    if (port != aPort) {
      ports.push(port);
    }
  }
  this._ports = ports;
}