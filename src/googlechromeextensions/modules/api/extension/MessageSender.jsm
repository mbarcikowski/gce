var EXPORTED_SYMBOLS = ["MessageSender"];

function MessageSender() {
  this._id = null;
  this._tab = null;
}

MessageSender.prototype.init = function(aId, aTab) {
  this._id = aId;
  this._tab = aTab;
  return this;
}

MessageSender.prototype.getWrapper = function getWrapper() {
  var wrapper = {};
  var messageSender = this;
  wrapper.id = messageSender._id;
  if (this._tab != null) {
    wrapper.tab = messageSender._tab.getWrapper();
  }
  else {
    wrapper.tab = null;
  }
  return wrapper;
}