Components.utils.import("resource://googlechromeextensions/ChromeEvent.jsm");

var EXPORTED_SYMBOLS = ["Port"];

function Port() {
  this._name = null;
  this._onDisconnect = null;
  this._onMessage = null;
  this._sender = null;
  this._view = null;
  this._views = new Array();
}

Port.prototype.init = function init(aGoogleChromeEngine, aView, aName, aSender) {
  this._view = aView;
  this._name = aName;
  this._onDisconnect = new ChromeEvent().init();
  this._onMessage = new ChromeEvent().init();
  this._sender = aSender;
  return this;
}

Port.prototype.getView = function getView() {
  return this._view;
}

Port.prototype.getOnMessage = function getOnMessage() {
  return this._onMessage;
}

Port.prototype.getOnDisconnect = function getOnDisconnect() {
  return this._onDisconnect;
}

Port.prototype._postMessage = function _postMessage(aView, aResponse) {
  this._onMessage.notify(aView, [JSON.parse(JSON.stringify(aResponse))]);
}

Port.prototype._disconnect = function _disconnect(aView) {
  var remove = false;
  if (this._view == aView) {
    this._onDisconnect.callbackNotify(aView,
        function(aPort) {
          return function(aListenerView) {
            return [aPort.initWrapper(aListenerView, false)];
          }
        }(this)
        );
    this.finalize();
    remove = true;
  }
  else {
    remove = this.releaseWrapper(aView);
  }
  if (remove){
    aView.getGoogleChromeEngine().getExtension().removePort(this);
    aView.getGoogleChromeEngine().getTabs().removePort(this);
  }
}

Port.prototype.initWrapper = function initWrapper(aView, aIncludeSender) {
  if (aView != this._view) {
    this._views.push(aView);
  }
  var wrapper = {};
  var port = this;
  wrapper.name = port._name;
  wrapper.onDisconnect = port._onDisconnect.initWrapper(aView);
  wrapper.onMessage = port._onMessage.initWrapper(aView);
  wrapper.postMessage = function(aPort, aInnerView) {
    return function postMessage(aResponse) {
      aPort._postMessage(aInnerView, aResponse);
    }
  }(this, aView);
  wrapper.disconnect = function(aPort, aInnerView) {
    return function disconnect(aResponse) {
      aPort._disconnect(aInnerView);
    }
  }(this, aView);
  if (aIncludeSender && this._sender != null) {
    wrapper.sender = port._sender.getWrapper();
  }
  return wrapper;
}

Port.prototype.releaseWrapper = function releaseWrapper(aView) {
  var views = new Array();
  for (var i = 0, l = this._views.length; i < l; i++) {
    var view = this._views[i];
    if (view != aView) {
      views.push(view);
    }
  }
  this._onMessage.releaseWrapper(aView);
  this._onDisconnect.releaseWrapper(aView);
  this._views = views;
  var remove = (this._views.length == 0);
  if (remove) {
    this._onDisconnect.callbackNotify(aView,
        function(aPort) {
          return function(aListenerView) {
            return [aPort.initWrapper(aListenerView, false)];
          }
        }(this)
        );
    this.finalize();
  }
  return remove;
}

Port.prototype.finalize = function finalize() {
  this._onDisconnect.finalize();
  this._onMessage.finalize();
}