//TODO get browser language
Components.utils.import("resource://googlechromeextensions/services/JSON.jsm");
Components.utils.import("resource://googlechromeextensions/services/FILE.jsm");
Components.utils.import("resource://googlechromeextensions/services/CONSOLE.jsm");

var EXPORTED_SYMBOLS = ["I18n"];

function I18n() {
  this._catalogs = {};
}

I18n.prototype._fileIID = Components.interfaces.nsIFile;
I18n.prototype._locales = "_locales";
I18n.prototype._localeCatalog = "messages.json";
I18n.prototype._messagePattern = "__MSG_[A-Za-z0-9]+__";
I18n.prototype._supportedLocales = {
  "am" : 1,
  "ar" : 1,
  "bg" : 1,
  "bn" : 1,
  "ca" : 1,
  "cs" : 1,
  "da" : 1,
  "de" : 1,
  "el" : 1,
  "en" : 1,
  "en_GB" : 1,
  "en_US" : 1,
  "es" : 1,
  "es_419" : 1,
  "et" : 1,
  "fi" : 1,
  "fil" : 1,
  "fr" : 1,
  "gu" : 1,
  "he" : 1,
  "hi" : 1,
  "hr" : 1,
  "hu" : 1,
  "id" : 1,
  "it" : 1,
  "ja" : 1,
  "kn" : 1,
  "ko" : 1,
  "lt" : 1,
  "lv" : 1,
  "ml" : 1,
  "mr" : 1,
  "nb" : 1,
  "nl" : 1,
  "or" : 1,
  "pl" : 1,
  "pt" : 1,
  "pt_BR" : 1,
  "pt_PT" : 1,
  "ro" : 1,
  "ru" : 1,
  "sk" : 1,
  "sl" : 1,
  "sr" : 1,
  "sv" : 1,
  "sw" : 1,
  "ta" : 1,
  "te" : 1,
  "th" : 1,
  "tr" : 1,
  "uk" : 1,
  "vi" : 1,
  "zh" : 1,
  "zh_CN" : 1,
  "zh_TW" : 1
}


I18n.prototype.init = function init(aExtensionDirectory, aManifest) {
  var localesDirectory = aExtensionDirectory.clone();
  localesDirectory.append(this._locales);
  if (localesDirectory.exists() && localesDirectory.isDirectory()) {
    if (aManifest.default_locale == null) {
      throw new Error("Localization used, but default_locale wasn't specified in the manifest.");
    }
    aManifest.default_locale = aManifest.default_locale + "";
    if (!this._supportedLocales[aManifest.default_locale]) {
      throw new Error("Supplied locale '" + aManifest.default_locale + "' is not supported.");
    }
    try {
      FILE.getFile(localesDirectory, aManifest.default_locale + "/" + this._localeCatalog);
    }
    catch(e) {
      throw new Error("Catalog file is missing for locale " + aManifest.default_locale + ".");
    }
    //TODO parse & check all file
    var entries = localesDirectory.directoryEntries;
    while (entries.hasMoreElements())
    {
      var entry = entries.getNext();
      entry.QueryInterface(this._fileIID);
      var locale = entry.leafName;
      if (entry.isDirectory() && locale[0] != ".") {
        if (!this._supportedLocales[locale]) {
          throw new Error("Supplied locale '" + locale + "' is not supported.");
        }
        var file = null;
        try {
          file = FILE.getFile(entry, this._localeCatalog);
        }
        catch(e) {
          throw new Error("Catalog file is missing for locale '" + locale + "'.");
        }
        var catalogContent = FILE.readFile(file);
        try {
          var catalog = JSON.decode(catalogContent);
        }
        catch(e) {
          throw new Error("Catalog for locale '" + locale + "' is not a valid JSON.");
        }
        if (catalog == null) {
          throw new Error("Catalog for locale '" + locale + "'is not a valid JSON.");
        }
        for (var key in catalog) {
          this._validEntry(key, catalog[key]);
        }
        this._catalogs[locale] = catalog;


      }
    }
  }
  else {
    if (aManifest.default_locale != null) {
      throw new Error("Default locale was specified, but _locales subtree is missing.");
    }
  }
}

//TODO
I18n.prototype._validEntry = function _validEntry(aKey, aValue) {
  /*
   "message": "Error: $details$",
   "description": "Generic error template. Expects error parameter to be passed in.",
   "placeholders": {
   "details": {
   "content": "$1",
   "example": "Failed to fetch RSS feed."
   }
   }

   */


  //There is "message" element for key 'error'
  //Not a valid tree for key 'zaz'
}

//TODO parseManifestString
I18n.prototype.parseManifestString = function parseManifestString(aString) {
  return aString;
  //throw new Error("Variable __MSG_messagename__ used but not defined.");
}

//TODO _getMessage
I18n.prototype._getMessage = function _getMessage(aMessageName, aSubstitutions) {
  return "";
}

//TODO _getAcceptLanguages
I18n.prototype._getAcceptLanguages = function _getAcceptLanguages(aCallback) {
  var languages = new Array();
  try {
    aCallback(languages);
  }
  catch(e) {
    CONSOLE.error(e);
  }
}

//TODO getWrapper
I18n.prototype.getWrapper = function getWrapper() {
  var wrapper = {};
  wrapper.getAcceptLanguages = function(aI18n) {
    return function(aCallback) {
      if (aCallback == null) {
        //TODO throw error?
      }
      aI18n._getAcceptLanguages(aCallback);
    }
  }(this);
  wrapper.getMessage = function(aI18n) {
    return function(aMessageName, aSubstitutions) {
      if (aMessageName == null) {
        //TODO throw error?
      }
      return aI18n._getMessage(aMessageName, aSubstitutions);
    }
  }(this);
  return wrapper;
}