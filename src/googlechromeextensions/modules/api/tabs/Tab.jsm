Components.utils.import("resource://googlechromeextensions/services/WINDOWS.jsm");

var EXPORTED_SYMBOLS = ["Tab"];

function Tab() {
  this._chromeTab = null;
  this._id = null;
}

Tab.prototype.init = function init(aChromeTab, aId) {
  this._chromeTab = aChromeTab;
  this._id = aId;
  return this;
}

Tab.prototype.getChromeTab = function getChromeTab() {
  return this._chromeTab;
}

Tab.prototype.getID = function getID() {
  return this._id;
}

Tab.prototype.getIndex = function getIndex() {
  return this._chromeTab._tPos;
}

Tab.prototype.getUrl = function getUrl() {
  return this._chromeTab.linkedBrowser.contentDocument.location.href;
}

Tab.prototype.getStatus = function getStatus() {
  if (this._chromeTab.linkedBrowser.contentWindow.document.readyState != "complete") {
    return "loading";
  }
  else {
    return "complete";
  }
}

Tab.prototype.getWindowId = function getWindowId() {
  return WINDOWS.findWindowId(this._chromeTab.ownerDocument.defaultView);
}

Tab.prototype.getFavIconUrl = function getFavIconUrl() {
  return this._chromeTab.image;
}

Tab.prototype.getTitle = function getTitle() {
  return this._chromeTab.linkedBrowser.contentDocument.title
}

Tab.prototype.isSelected = function isSelected() {
  return this._chromeTab.selected;
}

Tab.prototype.getChromeWindow = function getChromeWindow() {
  return this._chromeTab.ownerDocument.defaultView;
}


Tab.prototype.getWrapper = function getWrapper() {
  var wrapper = {};
  var tab = this;
  wrapper.id = tab.getID();
  wrapper.index = tab.getIndex();
  wrapper.selected = tab.isSelected();
  wrapper.url = tab.getUrl();
  wrapper.title = tab.getTitle();
  wrapper.favIconUrl = tab.getFavIconUrl();
  wrapper.windowId = tab.getWindowId();
  wrapper.status = tab.getStatus();
  return wrapper;
}

Tab.prototype.finalize = function finalize() {
  this._chromeTab = null;
  this._id = null;
}