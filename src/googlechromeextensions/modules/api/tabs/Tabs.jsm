//TODO connect to TABS service & WINDOWS Service

Components.utils.import("resource://googlechromeextensions/services/CONSOLE.jsm");
Components.utils.import("resource://googlechromeextensions/services/TABS.jsm");
Components.utils.import("resource://googlechromeextensions/api/extension/Port.jsm");
Components.utils.import("resource://googlechromeextensions/api/extension/MessageSender.jsm");
Components.utils.import("resource://googlechromeextensions/ContentScript.jsm");
Components.utils.import("resource://googlechromeextensions/ChromeEvent.jsm");

var EXPORTED_SYMBOLS = ["Tabs"];

function Tabs() {
  this._googleChromeEngine = null;
  this._onAttached = null;
  this._onCreated = null;
  this._onDetached = null;
  this._onMoved = null;
  this._onRemoved = null;
  this._onSelectionChanged = null;
  this._onUpdated = null;
  this._tabViewMap = {};
  this._tabContentScriptsMap = {};
  this._tabPortsMap = {};
}

Tabs.prototype._webProgressListenerIID = Components.interfaces.nsIWebProgressListener;

Tabs.prototype.init = function init(aGoogleChromeEngine) {
  this._googleChromeEngine = aGoogleChromeEngine;
  this._onAttached = new ChromeEvent().init();
  this._onCreated = new ChromeEvent().init();
  this._onDetached = new ChromeEvent().init();
  this._onMoved = new ChromeEvent().init();
  this._onRemoved = new ChromeEvent().init();
  this._onSelectionChanged = new ChromeEvent().init();
  this._onUpdated = new ChromeEvent().init();
  return this;
}

Tabs.prototype.getOnAttached = function getOnAttached() {
  return  this._onAttached;
}

Tabs.prototype.getOnCreated = function getOnCreated() {
  return  this._onCreated;
}

Tabs.prototype.getOnDetached = function getOnDetached() {
  return  this._onDetached;
}

Tabs.prototype.getOnMoved = function getOnMoved() {
  return  this._onMoved;
}

Tabs.prototype.getOnRemoved = function getOnRemoved() {
  return this._onRemoved;
}

Tabs.prototype.getOnSelectionChanged = function getOnSelectionChanged() {
  return  this._onSelectionChanged;
}

Tabs.prototype.getOnUpdated = function getOnUpdated() {
  return  this._onUpdated;
}


Tabs.prototype._createTab = function _createTab(aWindowId, aIndex, aUrl, aSelected, aCallback) {
  var url = aUrl == null ? "about:blank" : this._googleChromeEngine.convertToExtensionUrl(aUrl);
  TABS._createTab(aWindowId, aIndex, url, aSelected, aCallback);
}

Tabs.prototype.removeTab = function removeTab(aTabId, aCallback) {
  TABS.removeTab(aTabId, aCallback);
}

Tabs.prototype._getTabWrapper = function _getTabWrapper(aTabId) {
  return TABS._getTabWrapper(aTabId);
}

Tabs.prototype.getTabsWrappers = function getTabsWrappers(aWindowId) {
  return TABS.getTabsWrappers(aWindowId);
}

Tabs.prototype._getSelectedTabWrapper = function _getSelectedTabWrapper(aWindowId) {
  return TABS._getSelectedTabWrapper(aWindowId);
}

Tabs.prototype._getSelectedTab = function _getSelectedTab() {
  return TABS._getSelectedTab();
}

Tabs.prototype._moveTab = function _moveTab(aTabId, aWindowId, aIndex, aCallback) {
  TABS._moveTab(aTabId, aWindowId, aIndex, aCallback);
}

Tabs.prototype._updateTab = function _updateTab(aTabId, aUrl, aSelected, aCallback) {
  TABS._updateTab(aTabId, aUrl, aSelected, aCallback);
}

Tabs.prototype._captureVisibleTab = function _captureVisibleTab(aWindowId, aCallback) {
  TABS._captureVisibleTab(aWindowId, aCallback);
}

Tabs.prototype._executeContentScriptRules = function _executeContentScriptRules(aTab, aRunAt, aWhichFrames) {
  var rules = this._googleChromeEngine.getContentScriptRules();
  for (var i = 0, l = rules.length; i < l; i++) {
    var rule = rules[i];
    var windows = new Array();
    var window = aTab.getChromeTab().linkedBrowser.contentWindow;
    if (rule.matches(window.location.href)) {
      switch (aWhichFrames) {
        case "all":
          windows.push(window);
          if (rule.getAllFrames() == true) {
            var frames = window.document.defaultView.frames;
            for (var i = 0, l = frames.length; i < l; i++) {
              var frame = frames[i];
              if (rule.matches(frame.location.href)) {
                windows.push(frame);
              }
            }
          }
          break;
        case "frames":
          if (rule.getAllFrames() == true) {
            var frames = window.document.defaultView.frames;
            for (var i = 0, l = frames.length; i < l; i++) {
              var frame = frames[i];
              if (rule.matches(frame.location.href)) {
                windows.push(frame);
              }
            }
          }
          break;
        case "document":
          windows.push(window);
          break;
      }
    }
    var run = false;
    switch (aRunAt) {
      case "document_start" :
        run = rule.isRunAtStart();
        break;
      case "document_end" :
        var csses = rule.getCSS();
        for (var i_css = 0, l_css = csses.length; i_css < l_css; i_css++) {
          var content = this._googleChromeEngine.getFileContent(csses[i_css]);
          for (var j = 0, m = windows.length; j < m; j++) {
            var window = windows[j];
            this._insertCssContent(aTab, window, content);
          }
        }
        run = rule.isRunAtEnd();
        break;
      case "document_idle" :
        run = rule.isRunAtIdle();
        break;
    }
    if (run) {
      var jses = rule.getJS();
      for (var i_js = 0, l_js = jses.length; i_js < l_js; i_js++) {
        var content = this._googleChromeEngine.getFileContent(jses[i_js]);
        for (var j = 0, m = windows.length; j < m; j++) {
          var window = windows[j];
          this._insertJsContent(aTab, window, content);
        }
      }
    }
  }
}

Tabs.prototype._insertCSS = function _insertCSS(aTabId, aCode, aFile, aAllFrames, aCallback) {
  var tab = null;
  if (aTabId != null) {
    tab = this._getTab(aTabId);
  }
  else {
    tab = this._getSelectedTab();
  }
  if (tab == null) {
    //TODO error if not tab ?
    return;
  }
  var url = tab.getUrl();
  if (this._googleChromeEngine.matchesPermissions(url)) {
    var windows = new Array();
    var window = tab.getChromeTab().linkedBrowser.contentWindow;
    windows.push(window);
    if (aAllFrames == true) {
      var frames = window.document.defaultView.frames;
      for (var i = 0, l = frames.length; i < l; i++) {
        var frame = frames[i];
        windows.push(frame);
      }
    }
    var styleContent = null;
    if (aCode != null) {
      styleContent = aCode;
    }
    else {
      try {
        styleContent = this._googleChromeEngine.getFileContent(aFile);
      }
      catch(e) {
        CONSOLE.error("Error during tabs.insertCSS: Failed to load file: \"" + aFile + "\".");
      }
    }
    for (var i = 0, l = windows.length; i < l; i++) {
      this._insertCssContent(tab, windows[i], styleContent);
    }
  }
  else {
    CONSOLE.error("Error during tabs.insertCSS: Cannot access contents of url \"" + url + "\". Extension manifest must request permission to access this host.");
  }

  if (aCallback != null) {
    try {
      aCallback.call(aCallback);
    }
    catch(e) {
      CONSOLE.error(e);
    }
  }
}


Tabs.prototype._insertCssContent = function _insertCssContent(aTab, aWindow, aStyleContent) {
  if (aStyleContent != null) {
    var document = aWindow.document;
    var documentElement = document.documentElement;
    var style = document.createElement("style");
    style.innerHTML = aStyleContent;
    documentElement.appendChild(style);
  }
}

Tabs.prototype._executeScript = function _executeScript(aTabId, aCode, aFile, aAllFrames, aCallback) {
  var tab = null;
  if (aTabId != null) {
    tab = this._getTab(aTabId);
  }
  else {
    tab = this._getSelectedTab();
  }
  if (tab == null) {
    //TODO error if not tab ?
    return;
  }

  var url = tab.getUrl();
  if (this._googleChromeEngine.matchesPermissions(url)) {
    var windows = new Array();
    var window = tab.getChromeTab().linkedBrowser.contentWindow;
    windows.push(window);
    if (aAllFrames == true) {
      var frames = window.document.defaultView.frames;
      for (var i = 0, l = frames.length; i < l; i++) {
        var frame = frames[i];
        windows.push(frame);
      }
    }
    var codeContent = null;
    if (aCode != null) {
      codeContent = aCode;
    }
    else {
      try {
        codeContent = this._googleChromeEngine.getFileContent(aFile);
      }
      catch(e) {
        CONSOLE.error("Error during tabs.executeScript: Failed to load file: \"" + aFile + "\".");
      }
    }

    for (var i = 0, l = windows.length; i < l; i++) {
      this._insertJsContent(tab, windows[i], codeContent);
    }
  }
  else {
    CONSOLE.error("Error during tabs.executeScript: Cannot access contents of url \"" + url + "\". Extension manifest must request permission to access this host.");
  }

  if (aCallback != null) {
    try {
      aCallback.call(aCallback);
    }
    catch(e) {
      CONSOLE.error(e);
    }
  }
}

Tabs.prototype._insertJsContent = function _insertJsContent(aTab, aWindow, aCodeContent) {
  if (aCodeContent != null) {
    var contentScripts = this._tabContentScriptsMap[aTab.getID()]
    if (contentScripts == null) {
      contentScripts = new Array();
      this._tabContentScriptsMap[aTab.getID()] = contentScripts;
    }
    var contentScript = new ContentScript().init(this._googleChromeEngine, aWindow, aTab);
    contentScripts.push(contentScript);
    contentScript.executeScript(aCodeContent);
  }
}

Tabs.prototype._connect = function _connect(aView, aTabId, aName) {
  var messageSender = new MessageSender().init(this._googleChromeEngine.getID(), null);

  var ports = this._tabPortsMap[aTabId];
  if (ports == null) {
    ports = new Array();
    this._tabPortsMap[aTabId] = ports;
  }
  var port = new Port().init(this._googleChromeEngine, aView, aName, messageSender);
  ports.push(port);
  var isNotify = false;


  var contentScripts = this._tabContentScriptsMap[aTabId];
  if (contentScripts != null) {
    for (var i = 0, l = contentScripts.length; i < l; i++) {
      isNotify = isNotify || contentScripts[i].getOnConnect().callbackNotify(aView,
          function(aPort) {
            return function(aListenerView) {
              return [aPort.initWrapper(aListenerView, true)];
            }
          }(port)
          );
    }
  }

  var view = this._tabViewMap[aTabId];
  if (view != null) {
    isNotify = isNotify || this._googleChromeEngine.getExtension().getOnConnect().callbackNotify(aView,
        function(aPort, aInnerView) {
          return function(aListenerView) {
            if (aListenerView == aInnerView) {
              return [aPort.initWrapper(aListenerView, true)];
            }
            else {
              return null;
            }
          }
        }(port, view)
        );
  }
  if (!isNotify)
  {
    aView.getChromeWindow().setTimeout(function(aPort) {
      return function() {
        aPort.releaseWrapper(null);
      }
    }(port), 0);
  }
  return port.initWrapper(aView, false);
}

Tabs.prototype._sendRequest = function _sendRequest(aView, aTabId, aRequest, aResponseCallback) {
  var messageSender = new MessageSender().init(this._googleChromeEngine.getID(), null);
  var contentScripts = this._tabContentScriptsMap[aTabId];
  if (contentScripts != null) {
    for (var i = 0, l = contentScripts.length; i < l; i++) {
      var port = new Port().init(this._googleChromeEngine, aView, null, messageSender);
      var portSource = port.initWrapper(aView, false);
      var portTarget = port.initWrapper(contentScripts[i], true);
      if (aResponseCallback != null) {
        portSource.onMessage.addListener(aResponseCallback);
      }
      contentScripts[i].getOnRequest().notify(aView, [aRequest,
        portTarget.sender, function(aResponse) {
          portTarget.postMessage(aResponse);
        }]);
      port.finalize();
    }
  }

  var view = this._tabViewMap[aTabId];
  if (view != null) {
    var port = new Port().init(this._googleChromeEngine, aView, null, messageSender);
    var portSource = port.initWrapper(aView, false);
    if (aResponseCallback != null) {
      portSource.onMessage.addListener(aResponseCallback);
    }
    this._googleChromeEngine.getExtension().getOnRequest().callbackNotify(aView,
        function(aPort, aInnerView) {
          return function(aListenerView) {
            if (aListenerView == aInnerView) {
              var portTarget = aPort.initWrapper(aListenerView, true);
              return [aRequest,
                portTarget.sender, function(aResponse) {
                  portTarget.postMessage(aResponse);
                }];
            }
            else {
              return null;
            }
          }
        }(port, view)
        );
    port.finalize();
  }
}

Tabs.prototype.initWrapper = function initWrapper(aView) {
  var wrapper = {
    captureVisibleTab : function(aTabs) {
      return function captureVisibleTab(aWindowId, aCallback) {
        if (aCallback == null) {
          //TODO throw error ?
        }
        aTabs._captureVisibleTab(aWindowId, aCallback);
      }
    }(this),

    connect : function(aTabs, aInnerView) {
      return function connect(aTabId, aConnectInfo) {
        if (aTabId == null) {
          //TODO throw error ?
        }
        var name = null;
        if (aConnectInfo != null) {
          name = aConnectInfo.name;
        }
        return aTabs._connect(aInnerView, aTabId, name);
      }
    }(this, aView),

    create : function(aTabs) {
      return function create(aCreateProperties, aCallback) {
        if (aCreateProperties == null) {
          //TODO throw error ?
        }
        aTabs._createTab(aCreateProperties.windowId, aCreateProperties.index, aCreateProperties.url, aCreateProperties.selected, aCallback);
      }
    }(this),

    //TODO Not implemented
    detectLanguage: function(aTabs) {
      return function detectLanguage(aTabId, aCallback) {
        throw new Error("Not implemented");
      }
    }(this),

    executeScript : function(aTabs) {
      return function executeScript(aTabId, aDetails, aCallback) {
        if (aDetails == null || (aDetails.code != null && aDetails.file != null)) {
          //TODO throw error?
        }
        aTabs._executeScript(aTabId, aDetails.code, aDetails.file, aDetails.allFrames, aCallback);
      }
    }(this),

    get : function(aTabs) {
      return function get(aTabId, aCallback) {
        if (aTabId == null || aCallback == null) {
          //TODO throw error?
        }
        try {
          aCallback(aTabs._getTabWrapper(aTabId));
        }
        catch(e) {
          CONSOLE.error(e);
        }
      }
    }(this),

    getAllInWindow : function(aTabs) {
      return function getAllInWindow(aWindowId, aCallback) {
        if (aCallback == null) {
          //TODO throw error?
        }
        try {
          aCallback(aTabs.getTabsWrappers(aWindowId));
        }
        catch(e) {
          CONSOLE.error(e);
        }
      }
    }(this),

    getSelected : function(aTabs) {
      return function getSelected(aWindowId, aCallback) {
        if (aCallback == null) {
          //TODO throw error?
        }
        try {
          aCallback(aTabs._getSelectedTabWrapper(aWindowId));
        }
        catch(e) {
          CONSOLE.error(e);
        }
      }
    }(this),

    insertCSS : function(aTabs) {
      return function insertCSS(aTabId, aDetails, aCallback) {
        if (aDetails == null || (aDetails.code != null && aDetails.file != null)) {
          //TODO throw error?
        }
        aTabs._insertCSS(aTabId, aDetails.code, aDetails.file, aDetails.allFrames, aCallback);
      }
    }(this),

    move : function(aTabs) {
      return function move(aTabId, aMoveProperties, aCallback) {
        if (aTabId == null || aMoveProperties == null || aMoveProperties.index == null) {
          //TODO throw error?
        }
        aTabs._moveTab(aTabId, aMoveProperties.windowId, aMoveProperties.index, aCallback);
      }
    }(this),

    remove : function(aTabs) {
      return function remove(aTabId, aCallback) {
        if (aTabId == null) {
          //TODO throw error ?
        }
        aTabs.removeTab(aTabId, aCallback);
      }
    }(this),

    sendRequest : function(aTabs, aInnerView) {
      return function sendRequest(aTabId, aRequest, aResponseCallback) {
        if (aTabId == null || aRequest == null) {
          //TODO throw error ?
        }
        aTabs._sendRequest(aInnerView, aTabId, aRequest, aResponseCallback);
      }
    }(this, aView),

    update : function(aTabs) {
      return function update(aTabId, aUpdateProperties, aCallback) {
        if (aTabId === null || aUpdateProperties == null || (aUpdateProperties.url == null && aUpdateProperties.selected == null )) {
          //TODO throw error ?
        }
        aTabs._updateTab(aTabId, aUpdateProperties.url, aUpdateProperties.selected, aCallback);
      }
    }(this),

    //TODO Not implemented
    onAttached :  function (aTabs, aInnerView) {
      return aTabs.getOnAttached().initWrapper(aInnerView);
    }(this, aView),

    onCreated :  function (aTabs, aInnerView) {
      return aTabs.getOnCreated().initWrapper(aInnerView);
    }(this, aView),

    //TODO Not implemented
    onDetached  :  function (aTabs, aInnerView) {
      return aTabs.getOnDetached().initWrapper(aInnerView);
    }(this, aView),

    onMoved  :  function (aTabs, aInnerView) {
      return aTabs.getOnMoved().initWrapper(aInnerView);
    }(this, aView),

    onRemoved  :  function (aTabs, aInnerView) {
      return aTabs.getOnRemoved().initWrapper(aInnerView);
    }(this, aView),

    onSelectionChanged  :  function (aTabs, aInnerView) {
      return aTabs.getOnSelectionChanged().initWrapper(aInnerView);
    }(this, aView),

    onUpdated :  function (aTabs, aInnerView) {
      return aTabs.getOnUpdated().initWrapper(aInnerView);
    }(this, aView)
  };
  return wrapper;
}

Tabs.prototype._finalizeTab = function (aTabId) {
  var view = this._tabViewMap[aTabId];
  if (view != null) {
    this._googleChromeEngine.getViews().removeView(view);
    delete (this._tabViewMap[aTabId]);
  }

  var contentScripts = this._tabContentScriptsMap[aTabId];
  if (contentScripts != null) {
    for (var i = 0, l = contentScripts.length; i < l; i++) {
      contentScripts[i].finalize();
    }
    delete(this._tabContentScriptsMap[aTabId]);
  }

  var ports = this._tabPortsMap[aTabId];
  if (ports != null) {
    for (var i = 0, l = ports.length; i < l; i++) {
      var port = ports[i];
      port.getOnDisconnect().callbackNotify(null,
          function(aPort) {
            return function(aListenerView) {
              return [aPort.initWrapper(aListenerView, false)];
            }
          }(port)
          );
      port.finalize();
    }
    delete (this._tabPortsMap[aTabId]);
  }
}

Tabs.prototype._onLocationChanged = function _onLocationChanged(aTab, aWindow) {
  if (aWindow != null) {
    this._finalizeTab(aTab.getID());

    if (aWindow.location.href.indexOf(this._googleChromeEngine.getExtensionUrl()) == 0) {
      var view = this._googleChromeEngine.getViews().addView(aWindow);
      view.setTab(aTab);
      this._tabViewMap[aTab.getID()] = view;
      var window = new XPCSafeJSObjectWrapper(aWindow);
      var chrome = this._googleChromeEngine.initChromeWrapper(view);
      window['chrome'] = chrome;
      window['console'] = CONSOLE.getWrapper();
    }

  }
}

Tabs.prototype.removePort = function removePort(aPort) {
  for (var id in this._tabPortsMap) {
    var ports = this._tabPortsMap[id];
    var newPorts = new Array();
    for (var i = 0, l = ports.length; i < l; i++) {
      var port = ports[i];
      if (port != aPort) {
        ports.push(port);
      }
    }
    this._tabPortsMap[id] = ports;
  }
}

Tabs.prototype.releaseWrapper = function releaseWrapper(aView) {
  for (var id in this._tabPortsMap) {
    var ports = this._tabPortsMap[id];
    var newPorts = new Array();
    for (var i = 0, l = ports.length; i < l; i++) {
      var port = ports[i];
      if (port.getView() == aView) {
        port.getOnDisconnect().callbackNotify(aView,
            function(aPort) {
              return function(aListenerView) {
                return [aPort.initWrapper(aListenerView, false)];
              }
            }(port)
            );
        port.finalize();
      }
      else {
        if (!port.releaseWrapper(aView)) {
          newPorts.push(port);
        }
      }
    }
    this._tabPortsMap[id] = newPorts;
  }


  this._onAttached.releaseWrapper(aView);
  this._onCreated.releaseWrapper(aView);
  this._onDetached.releaseWrapper(aView);
  this._onMoved.releaseWrapper(aView);
  this._onRemoved.releaseWrapper(aView);
  this._onSelectionChanged.releaseWrapper(aView);
  this._onUpdated.releaseWrapper(aView);
}

Tabs.prototype.onNotification = function onNotification(aEvent, aArgs) {
  switch (aEvent) {
    case TABS.ON_REMOVED_EVENT :
      this._onRemoved.notify(null, aArgs);
      this._finalizeTab(aArgs[0]);
      break;
    case TABS.ON_CREATED_EVENT :
      this._onCreated.notify(null, aArgs);
      break;
    case TABS.ON_MOVED_EVENT :
      this._onMoved.notify(null, aArgs);
      break;
    case TABS.ON_LOCATION_CHANGED_EVENT :
      var tab = aArgs[0];
      var window = aArgs[1];
      this._onLocationChanged(tab, window);
      break;
    case TABS.ON_DOCUMENT_START_EVENT :
      var tab = aArgs[0];
      var url = tab.getUrl();
      this._executeContentScriptRules(tab, "document_start", "all");
      this._onUpdated.notify(null, [tab.getID(),
        {
          status : tab.getStatus(),
          url : url
        }, tab.getWrapper()]);
      break;
    case TABS.ON_DOCUMENT_END_EVENT :
      var tab = aArgs[0];
      this._executeContentScriptRules(tab, "document_end", "document");
      break;
    case TABS.ON_FRAMES_END_EVENT :
      var tab = aArgs[0];
      this._executeContentScriptRules(tab, "document_end", "frames");
      break;
    case TABS.ON_DOCUMENT_IDLE_EVENT :
      var tab = aArgs[0];
      this._executeContentScriptRules(tab, "document_idle", "all");
      break;
    case TABS.ON_STATE_CHANGED_EVENT :
      var tab = aArgs[0];
      this._onUpdated.notify(null, [tab.getID(),
        {
          status : tab.getStatus()
        }, tab.getWrapper()]);
      break;
    case TABS.ON_SELECTION_CHANGED_EVENT :
      this._onSelectionChanged.notify(null, aArgs);
      break;
  }
}
