var EXPORTED_SYMBOLS = ["Window"];

function Window() {
  this._windows = null;
  this._chromeWindow = null;
  this._id = null;
}

Window.prototype.init = function init(aWindows, aChromeWindow, aId) {
  this._windows = aWindows;
  this._chromeWindow = aChromeWindow;
  this._id = aId;
  return this;
}

Window.prototype.getChromeWindow = function getChromeWindow() {
  return this._chromeWindow;
}

Window.prototype.getID = function getID() {
  return this._id;
}

Window.prototype.getWrapper = function getWrapper() {
  var wrapper = {};
  var window = this;
  wrapper.id = window._id;
  wrapper.focused = (window._windows._getLastFocusedWindow() == this);
  wrapper.top = window._chromeWindow.screenY;
  wrapper.left = window._chromeWindow.screenX;
  wrapper.width = window._chromeWindow.innerWidth;
  wrapper.height = window._chromeWindow.innerHeight;
  return wrapper;
}

Window.prototype.finalize = function finalize() {
  this._chromeWindow = null;
  this._id = null;
}