//TODO connect to WINDOWS service
Components.utils.import("resource://googlechromeextensions/ChromeEvent.jsm");
Components.utils.import("resource://googlechromeextensions/api/windows/Window.jsm");
Components.utils.import("resource://googlechromeextensions/services/CONSOLE.jsm");
Components.utils.import("resource://googlechromeextensions/services/WINDOWS.jsm");

var EXPORTED_SYMBOLS = ["Windows"];

function Windows() {
  this._googleChromeEngine = null;
  this._onCreated = null;
  this._onFocusChanged = null;
  this._onRemoved = null;
}

Windows.prototype.init = function init(aGoogleChromeEngine) {
  this._googleChromeEngine = aGoogleChromeEngine;
  this._onCreated = new ChromeEvent().init();
  this._onFocusChanged = new ChromeEvent().init();
  this._onRemoved = new ChromeEvent().init();
  return this;
}

Windows.prototype.getOnCreated = function getOnCreated() {
  return this._onCreated;
}

Windows.prototype.getOnFocusChanged = function getOnFocusChanged() {
  return this._onFocusChanged;
}

Windows.prototype.getOnRemoved = function getOnRemoved() {
  return this._onRemoved;
}

Windows.prototype._createWindow = function _createWindow(aUrl, aLeft, aTop, aWidth, aHeight, aCallback) {
  var url = this._googleChromeEngine.convertToExtensionUrl(aUrl);
	WINDOWS._createWindow(url, aLeft, aTop, aWidth, aHeight, aCallback);
}

Windows.prototype._removeWindowId = function _removeWindowId(aWindowId, aCallback) {
	WINDOWS._removeWindowId(aWindowId, aCallback);
}

Windows.prototype._updateWindowId = function _updateWindowId(aWindowId, aLeft, aTop, aWidth, aHeight, aCallback) {
	WINDOWS._updateWindowId(aWindowId, aLeft, aTop, aWidth, aHeight, aCallback);
}


Windows.prototype.getCurrentWindow = function getCurrentWindow() {
  return this._getLastFocusedWindow();
}

Windows.prototype._getLastFocusedWindow = function _getLastFocusedWindow() {
  return WINDOWS._getLastFocusedWindow();
}

Windows.prototype._getWindowWrapper = function _getWindowWrapper(aId) {
  return WINDOWS._getWindowWrapper(aId);
}

Windows.prototype._getAllWindowWrappers = function _getAllWindowWrappers(aPopulate) {
    return WINDOWS._getAllWindowWrappers(aPopulate);
}

Windows.prototype.initWrapper = function initWrapper(aView) {
  var wrapper = {
    create : function(aWindows) {
      return function create(aCreateData, aCallback) {
        if (aCreateData != null) {
          aWindows._createWindow(aCreateData.url, aCreateData.left, aCreateData.top, aCreateData.width, aCreateData.height, aCallback);
        }
        else {
          aWindows._createWindow(null, null, null, null, null, aCallback);
        }
      }
    }(this),

    get : function(aWindows) {
      return function get(aWindowId, aCallback) {
        aCallback(aWindows._getWindowWrapper(aWindowId));
      }
    }(this),

    getAll : function(aWindows) {
      return function getAll(aGetInfo, aCallback) {
        var windows = aWindows._getAllWindowWrappers((aGetInfo != null && aGetInfo.populate == true));
        aCallback(windows);
      }
    }(this),

    getCurrent : function(aWindows) {
      return function getCurrent(aCallback) {
        aCallback(aWindows._getLastFocusedWindow().getWrapper());
      }
    }(this),

    getLastFocused : function(aWindows) {
      return function getLastFocused(aCallback) {
        aCallback(aWindows._getLastFocusedWindow().getWrapper());
      }
    }(this),

    remove : function(aWindows) {
      return function remove(aWindowId, aCallback) {
        aWindows._removeWindowId(aWindowId, aCallback);
      }
    }(this),

    update : function(aWindows) {
      return function update(aWindowId, aUpdateInfo, aCallback) {
        if (aUpdateInfo != null) {
          aWindows._updateWindowId(aWindowId, aUpdateInfo.left, aUpdateInfo.top, aUpdateInfo.width, aUpdateInfo.height, aCallback);
        }
        else {
          aWindows._updateWindowId(aWindowId, null, null, null, null, aCallback);
        }
      }
    }(this),

    onCreated : function(aWindows, aInnerView) {
      return aWindows.getOnCreated().initWrapper(aInnerView);
    }(this, aView),

    onFocusChanged : function(aWindows, aInnerView) {
      return aWindows.getOnFocusChanged().initWrapper(aInnerView);
    }(this, aView),

    onRemoved : function(aWindows, aInnerView) {
      return aWindows.getOnRemoved().initWrapper(aInnerView);
    }(this, aView)
  };
  return wrapper;
}

Windows.prototype.onNotification = function onNotification(aEvent, aArgs) {
  switch (aEvent) {
    case WINDOWS.ON_REMOVED_EVENT :      
      this._onRemoved.notify(null, aArgs);
      break;
    case WINDOWS.ON_CREATED_EVENT :
      this._onCreated.notify(null, aArgs);
      break;
    case WINDOWS.ON_FOCUS_CHANGED_EVENT :
      this._onFocusChanged.notify(null, [aArgs]);
      break;
  }
}

Windows.prototype.releaseWrapper = function releaseWrapper(aView) {
  this._onCreated.releaseWrapper(aView);
  this._onFocusChanged.releaseWrapper(aView);
  this._onRemoved.releaseWrapper(aView);
}
