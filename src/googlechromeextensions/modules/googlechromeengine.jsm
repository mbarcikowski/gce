Components.utils.import("resource://googlechromeextensions/services/CONSOLE.jsm");
Components.utils.import("resource://googlechromeextensions/services/FILE.jsm");
Components.utils.import("resource://googlechromeextensions/services/JSON.jsm");
Components.utils.import("resource://googlechromeextensions/services/HIDDENWINDOW.jsm");
Components.utils.import("resource://googlechromeextensions/services/WINDOWS.jsm");
Components.utils.import("resource://googlechromeextensions/services/TABS.jsm");
Components.utils.import("resource://googlechromeextensions/ChromeExtension.jsm");

var EXPORTED_SYMBOLS = ["googlechromeengine"];

function GoogleChromeEngine() {
  this._extensions = {};
  this._console = null;
  this._hiddenWindow = null;
  this._windows = null;
  this._tabs = null;
}

GoogleChromeEngine.prototype._guid = "googlechromeextensions@matoeil.fr";
GoogleChromeEngine.prototype._manifest = "manifest.json";
GoogleChromeEngine.prototype._extmanagerCID = '@mozilla.org/extensions/manager;1';
GoogleChromeEngine.prototype._extmanagerIID = Components.interfaces.nsIExtensionManager;
GoogleChromeEngine.prototype._fileIID = Components.interfaces.nsIFile;
GoogleChromeEngine.prototype._extensionsPath = "extensions/";


GoogleChromeEngine.prototype.init = function init() {
  this._console = CONSOLE;
  this._tabs = TABS;
  this._windows = WINDOWS;
  this._windows.setTabs(this._tabs);
  this._tabs.setWindows(this._windows);
  this._windows.setExtensions(this._extensions);
  this._tabs.setExtensions(this._extensions);
  this._hiddenWindow = HIDDENWINDOW;
  this._loadExtensions();
  this._startExtensions();
  return this;
}

GoogleChromeEngine.prototype._loadExtensions = function _loadExtensions() {
  var em = Components.classes[this._extmanagerCID].getService(this._extmanagerIID);
  var extensionsDirectory = em.getInstallLocation(this._guid).getItemFile(this._guid, this._extensionsPath);
  var entries = extensionsDirectory.directoryEntries;
  while (entries.hasMoreElements())
  {
    var entry = entries.getNext();
    entry.QueryInterface(this._fileIID);
    if (entry.isDirectory()) {
      var manifestFile = entry.clone();
      manifestFile.append(this._manifest);
      if (manifestFile.exists()) {
        var manifestContent = FILE.readFile(manifestFile);
        try {
          var extension = new ChromeExtension().init(entry, manifestContent);
          this._extensions[extension.getID()] = extension;
          this._console.error("extension '" + extension.getID() + "' initialized.");
        }
        catch(e) {
          this._console.error("Could not load extension from '" + entry.path + "'. " + e.message);
        }
      }
    }
  }
}

GoogleChromeEngine.prototype._startExtensions = function _startExtensions() {
  for (var id in this._extensions) {
    this._startExtension(this._extensions[id]);
  }
}

GoogleChromeEngine.prototype._startExtension = function _startExtension(aExtension) {
  this._hiddenWindow.loadBackGroundPage(aExtension, aExtension.getBackgroundPage());
}

GoogleChromeEngine.prototype.findExtension = function findExtension(aID) {
  return this._extensions[aID];
}

GoogleChromeEngine.prototype.getExtensions = function getExtensions(){
	return this._extensions;	
}

var googlechromeengine;
try {
  googlechromeengine = new GoogleChromeEngine().init();
} catch(e) {
  CONSOLE.error(e);
}



