var EXPORTED_SYMBOLS = ["CONSOLE"];

function Console() {
  this._consoleService = null;
}

Console.prototype._consoleserviceCID = '@mozilla.org/consoleservice;1';
Console.prototype._consoleserviceIID = Components.interfaces.nsIConsoleService;
Console.prototype._scripterrorCID = '@mozilla.org/scripterror;1';
Console.prototype._scripterrorIID = Components.interfaces.nsIScriptError;

Console.prototype.init = function init() {
  this._consoleService = Components.classes[this._consoleserviceCID].getService(this._consoleserviceIID);
  return this;
}

Console.prototype.getWrapper = function getWrapper() {
  var wrapper = {};
  wrapper.log = function(aConsole) {
    return function log(aMessage) {
      aConsole._warn(aMessage, Components.stack.caller);
    }
  }(this);
  wrapper.error = function(aConsole) {
    return function error(aMessage) {
      aConsole._error(aMessage, Components.stack.caller);
    }
  }(this);
  return wrapper;
}

Console.prototype.error = function error(aMessage) {
  this._error(aMessage, Components.stack.caller);
}

Console.prototype.log = function log(aMessage) {
  this._warn(aMessage, Components.stack.caller);
}


Console.prototype._log = function _log(aMessage, aCaller, aFlag) {
  if (aFlag != null) {
    var filename = null;
    var lineNumber = null;
    if (aCaller) {
      filename = aCaller.filename;
      lineNumber = aCaller.lineNumber;
    }
    var scriptError = Components.classes[this._scripterrorCID].createInstance(this._scripterrorIID);
    scriptError.init(aMessage, filename, null, lineNumber, null, aFlag, "");
    this._consoleService.logMessage(scriptError);
  }
  else {
    this._consoleService.logStringMessage(aMessage);
  }
}

Console.prototype._warn = function _warn(aMessage, aCaller) {
  this._log(aMessage, aCaller, this._scripterrorIID.warnFlag);
}

Console.prototype._error = function _error(aMessage, aCaller) {
  if (typeof aMessage == "string") {
    this._log(aMessage, aCaller, this._scripterrorIID.errorFlag);
  }
  else {
    if (aMessage != null) {
      var message = new Array();
      if (aMessage.message != null) {
        message.push("message : " + aMessage.message + "\n");
      }
      if (aMessage.name != null) {
        message.push("name : " + aMessage.name + "\n");
      }
      if (aMessage.description != null) {
        message.push("description : " + aMessage.description + "\n");
      }
      if (message.length == 0) {
        message.push("raw : " + aMessage.toString() + "\n");
      }
      if (aMessage.stack != null) {
        message.push("stack : " + aMessage.stack + "\n");
      }
      else {
        message.push("stack : " + this._parseStack(aCaller) + "\n");
      }
      var caller = {
        filename : aMessage.fileName != null ? aMessage.fileName : (aCaller != null ? aCaller.filename : null),
        lineNumber : aMessage.lineNumber != null ? aMessage.lineNumber : (aCaller != null ? aCaller.lineNumber : null)
      }
      this._log(message.join(""), caller, this._scripterrorIID.errorFlag);
    }
  }
}

Console.prototype._parseStack = function _parseStack(aStack) {
  var res = new Array();
  while (aStack != null) {
    res.push(" - " + aStack.filename + "@" + aStack.lineNumber + " : \n");
    aStack = aStack.caller;
  }
  return res.join("");
}

var CONSOLE = new Console().init();