var EXPORTED_SYMBOLS = ["FILE"];

var FILE = {
  _charset : "UTF-8",
  _scriptableinputstreamCID : "@mozilla.org/scriptableinputstream;1",
  _scriptableinputstreamIID : Components.interfaces.nsIScriptableInputStream,
  _fileinputstreamCID : "@mozilla.org/network/file-input-stream;1",
  _fileinputstreamIID : Components.interfaces.nsIFileInputStream,
  _scriptableunicodeconverterCID : "@mozilla.org/intl/scriptableunicodeconverter",
  _scriptableunicodeconverterIID : Components.interfaces.nsIScriptableUnicodeConverter
}

FILE.getFile = function getFile(aDirectory, aFilePath) {
  var leafNames = aFilePath.split("/");
  var file = aDirectory.clone();
  for (var i = 0, l = leafNames.length; i < l; i++) {
    file.append(leafNames[i]);
  }
  if (!file.exists() || !file.isFile()) {
    throw new Error("File not found.");
  }
  return file;
}

FILE.readFile = function readFile(aFile) {
  try {
    var data = new String();
    var fiStream = Components.classes[this._fileinputstreamCID].createInstance(this._fileinputstreamIID);
    var siStream = Components.classes[this._scriptableinputstreamCID].createInstance(this._scriptableinputstreamIID);
    fiStream.init(aFile, 1, 0, false);
    siStream.init(fiStream);
    data += siStream.read(-1);
    siStream.close();
    fiStream.close();
    if (this._charset) {
      data = this._toUnicode(this._charset, data);
    }
    return data;
  }
  catch(e) {
    return null;
  }
}

FILE._toUnicode = function _toUnicode(aCharset, aData) {
  try {
    var uniConv = Components.classes[this._scriptableunicodeconverterCID].createInstance(this._scriptableunicodeconverterIID);
    uniConv.charset = aCharset;
    aData = uniConv.ConvertToUnicode(aData);
  }
  catch(e) {
    return null;
  }
  return aData;
}    