var EXPORTED_SYMBOLS = ["HIDDENWINDOW"];

function HiddenWindow() {
  this._location = null;
  this._HiddenWindowLoaded = false;
  this._hiddenWindow = null;
  this._backgroundPages = new Array();
}

HiddenWindow.prototype.HIDDEN_IFRAME_URL = "resource://gre/res/hiddenWindow.html";
HiddenWindow.prototype._webProgressListenerIID = Components.interfaces.nsIWebProgressListener;
HiddenWindow.prototype._appshellserviceCID = "@mozilla.org/appshell/appShellService;1";
HiddenWindow.prototype._appshellserviceIID = Components.interfaces.nsIAppShellService;
HiddenWindow.prototype._webnavigationIID = Components.interfaces.nsIWebNavigation;
HiddenWindow.prototype._docshellIID = Components.interfaces.nsIDocShell;
HiddenWindow.prototype._interfacerequestorIID = Components.interfaces.nsIInterfaceRequestor;
HiddenWindow.prototype._webprogressIID = Components.interfaces.nsIWebProgress;
HiddenWindow.prototype._supportsweakreferenceIID = Components.interfaces.nsISupportsWeakReference;
HiddenWindow.prototype._observerIID = Components.interfaces.nsIObserver;
HiddenWindow.prototype._supportsIID = Components.interfaces.nsISupports;
HiddenWindow.prototype._nointerfaceRID = Components.results.NS_NOINTERFACE;


HiddenWindow.prototype.init = function init() {
  this._hiddenWindow = Components.classes[this._appshellserviceCID].getService(this._appshellserviceIID).hiddenDOMWindow;
  this._addProgressListener(this._getWebProgress(this._hiddenWindow));
  return this;
}


HiddenWindow.prototype.onLocationChange = function onLocationChange(webProgress, request, location) {
  this._HiddenWindowLoaded = false;
  this._location = location.spec;
}

HiddenWindow.prototype.onProgressChange = function onProgressChange(webProgress, request, curSelfProgress) {
}

HiddenWindow.prototype.onSecurityChange = function onSecurityChange(webProgress, request, state) {
}

HiddenWindow.prototype.onStatusChange = function onStateChange(webProgress, request, aStatus, aMessage) {
}

HiddenWindow.prototype.onStateChange = function onStateChange(webProgress, request, stateFlags, status) {
  if ((this._webProgressListenerIID.STATE_STOP & stateFlags) && (this._webProgressListenerIID.STATE_IS_WINDOW & stateFlags)
      && (this._location == this.HIDDEN_IFRAME_URL) && !this._HiddenWindowLoaded) {
    this._HiddenWindowLoaded = true;
    this._removeProgressListener(this._getWebProgress(this._hiddenWindow));
    this._loadBackgroundPages();
  }
}


HiddenWindow.prototype.QueryInterface = function QueryInterface(aIID) {
  if (aIID.equals(this._webProgressListenerIID) ||
      aIID.equals(this._supportsweakreferenceIID) ||
      aIID.equals(this._observerIID) ||
      aIID.equals(this._supportsIID))
    return this;
  throw this._nointerfaceRID;
}

HiddenWindow.prototype.loadBackGroundPage = function loadBackGroundPage(aGoogleChromeEngine, abackgroundPage) {
  if (this._HiddenWindowLoaded) {
    this._loadBackgroundPage(aGoogleChromeEngine, abackgroundPage);
  }
  else {
    this._backgroundPages.push({googleChromeEngine : aGoogleChromeEngine, backgroundPage: abackgroundPage});
  }
}

HiddenWindow.prototype._loadBackgroundPages = function _loadBackgroundPages() {
  for (var i = 0, l = this._backgroundPages.length; i < l; i++) {
    var entry = this._backgroundPages[i];
    this._loadBackgroundPage(entry.googleChromeEngine, entry.backgroundPage);
  }
  this._backgroundPages = new Array();
}

HiddenWindow.prototype._loadBackgroundPage = function _loadBackgroundPage(aGoogleChromeEngine, aBackgroundPage) {
  var path = aBackgroundPage.getBackgroundPage();
  if (path != null) {
    var doc = this._hiddenWindow.document;
    var iframe = doc.createElement("iframe");
    iframe.setAttribute("id", aGoogleChromeEngine.getID());
    doc.lastChild.appendChild(iframe);
    var webProgress = this._getWebProgress(iframe.contentWindow);
    aBackgroundPage.addProgressListener(webProgress);
    iframe.contentDocument.location.href = aGoogleChromeEngine.convertToExtensionUrl(path);
  }
}

HiddenWindow.prototype.getLocalStorage = function getLocalStorage(aGoogleChromeEngine) {
  return  this._hiddenWindow.globalStorage[aGoogleChromeEngine.getID()];
}

HiddenWindow.prototype.getDataUrl = function getDataUrl(aImageData) {
  var doc = this._hiddenWindow.document;
  var canvas = doc.createElement("canvas");
  var canvasContext = canvas.getContext("2d");
  canvas.width = aImageData.width;
  canvas.height = aImageData.height;
  canvasContext.putImageData(aImageData, 0, 0);
  return canvas.toDataURL();
}

HiddenWindow.prototype.openWindow = function openWindow(aUrl, aLeft, aTop, aWidth, aHeight) {
  var features = new Array();
  if (aLeft != null) {
    features.push("left=" + aLeft);
  }
  if (aTop != null) {
    features.push("top=" + aTop);
  }
  if (aWidth != null) {
    features.push("width=" + aWidth);
  }
  if (aHeight != null) {
    features.push("height=" + aHeight);
  }
  features.push("menubar=yes");
  features.push("toolbar=yes");
  features.push("location=yes");
  features.push("personalbar=yes");
  features.push("resizable=yes");
  features.push("scrollbars=yes");
  features.push("status=yes");
  return this._hiddenWindow.open(aUrl, null, features.join(","));
}

HiddenWindow.prototype.getSnapshot = function getSnapshot(aChromeWindow) {
  var doc = this._hiddenWindow.document;
  var canvas = doc.createElement("canvas");
  var canvasContext = canvas.getContext("2d");
  canvas.width = aChromeWindow.innerWidth;
  canvas.height = aChromeWindow.innerHeight;
  canvasContext.drawWindow(aChromeWindow, aChromeWindow.scrollX, aChromeWindow.scrollY, aChromeWindow.innerWidth, aChromeWindow.innerHeight, "rgb(255,255,255)");
  return canvas.toDataURL();
}

HiddenWindow.prototype._getWebProgress = function _getWebProgress(aChromeWindow) {
  var webNavigation = aChromeWindow.QueryInterface(this._interfacerequestorIID).getInterface(this._webnavigationIID);
  var docShell = webNavigation.QueryInterface(this._docshellIID);
  return docShell.QueryInterface(this._interfacerequestorIID).getInterface(this._webprogressIID);
}

HiddenWindow.prototype._addProgressListener = function _addProgressListener(aWebProgress) {
  aWebProgress.addProgressListener(this, this._webprogressIID.NOTIFY_ALL);
}

HiddenWindow.prototype._removeProgressListener = function _removeProgressListener(aWebProgress) {
  aWebProgress.removeProgressListener(this, this._webprogressIID.NOTIFY_ALL);
}

var HIDDENWINDOW = new HiddenWindow().init();