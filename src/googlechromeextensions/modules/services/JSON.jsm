var EXPORTED_SYMBOLS = ["JSON"];

var _jsonCID = '@mozilla.org/dom/json;1';
var _jsonIID = Components.interfaces.nsIJSON;

var JSON = Components.classes[_jsonCID].createInstance(_jsonIID);