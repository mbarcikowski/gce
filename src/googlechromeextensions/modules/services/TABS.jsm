Components.utils.import("resource://googlechromeextensions/services/CONSOLE.jsm");
Components.utils.import("resource://googlechromeextensions/services/HIDDENWINDOW.jsm");
Components.utils.import("resource://googlechromeextensions/api/tabs/tab.jsm");

var EXPORTED_SYMBOLS = ["TABS"];

function Tabs() {
  this._tabOpenListener = null;
  this._tabMoveListener = null;
  this._tabCloseListener = null;
  this._tabSelectListener = null;
  this._tabsMap = {};
  this._currentTabId = -1;
  this._extensions = null;
  this._windows = null;
}

Tabs.prototype._webProgressListenerIID = Components.interfaces.nsIWebProgressListener;

Tabs.prototype.SERVICENAME = "TABS";

Tabs.prototype.ON_REMOVED_EVENT = "ON_REMOVED_EVENT";
Tabs.prototype.ON_CREATED_EVENT = "ON_CREATED_EVENT";
Tabs.prototype.ON_MOVED_EVENT = "ON_MOVED_EVENT";

Tabs.prototype.ON_LOCATION_CHANGED_EVENT = "ON_LOCATION_CHANGED_EVENT";
Tabs.prototype.ON_DOCUMENT_START_EVENT = "ON_FRAMES_END_EVENT";
Tabs.prototype.ON_DOCUMENT_END_EVENT = "ON_DOCUMENT_END_EVENT";
Tabs.prototype.ON_FRAMES_END_EVENT = "ON_FRAMES_END_EVENT";
Tabs.prototype.ON_DOCUMENT_IDLE_EVENT = "ON_DOCUMENT_IDLE_EVENT";

Tabs.prototype.ON_STATE_CHANGED_EVENT = "ON_LOCATION_CHANGED_EVENT";
Tabs.prototype.ON_SELECTION_CHANGED_EVENT = "ON_SELECTION_CHANGED_EVENT";

Tabs.prototype.init = function init() {
  this._tabOpenListener = function(aTabs) {
    return function() {
      aTabs._onTabOpen.apply(aTabs, arguments);
    }
  }(this);
  this._tabMoveListener = function(aTabs) {
    return function() {
      aTabs._onTabMove.apply(aTabs, arguments);
    }
  }(this);
  this._tabCloseListener = function(aTabs) {
    return function() {
      aTabs._onTabClose.apply(aTabs, arguments);
    }
  }(this);
  this._tabSelectListener = function(aTabs) {
    return function() {
      aTabs._onTabSelect.apply(aTabs, arguments);
    }
  }(this);
  return this;
}

Tabs.prototype.setWindows = function setWindows(aWindows) {
  this._windows = aWindows;
}

Tabs.prototype.setExtensions = function setExtensions(aExtensions) {
  this._extensions = aExtensions;
}

Tabs.prototype.getTabOpenListener = function getTabOpenListener() {
  return this._tabOpenListener;
}

Tabs.prototype.getTabMoveListener = function getTabMoveListener() {
  return this._tabMoveListener;
}

Tabs.prototype.getTabCloseListener = function getTabCloseListener() {
  return this._tabCloseListener;
}

Tabs.prototype.getTabSelectListener = function getTabSelectListener() {
  return this._tabSelectListener;
}

Tabs.prototype._getNewTabID = function _getNewTabID() {
  return ++this._currentTabId;
}

Tabs.prototype._findTab = function _findTab(aTab) {
  for (var id in this._tabsMap) {
    var tab = this._tabsMap[id];
    if (tab.getChromeTab() == aTab) {
      return tab;
    }
  }
  return null;
}

Tabs.prototype._getTab = function _getTab(aTabId) {
  return this._tabsMap[aTabId];
}

Tabs.prototype._addTab = function _addTab(aTab) {
  for (var id in this._tabsMap) {
    var tab = this._tabsMap[id];
    if (tab.getChromeTab() == aTab) {
      return tab;
    }
  }
  var id = this._getNewTabID();
  var tab = new Tab().init(aTab, id);
  this._tabsMap[id] = tab;
  this._notifyExtensions(this.ON_CREATED_EVENT, [tab.getWrapper()]);
  return tab;
}

Tabs.prototype._removeTab = function _removeTab(aTab) {
  var toRemoveTab = null;
  for (var id in this._tabsMap) {
    var tab = this._tabsMap[id];
    if (tab.getChromeTab() == aTab) {
      toRemoveTab = tab;
    }
  }
  if (toRemoveTab != null) {
    var id = toRemoveTab.getID();
    delete (this._tabsMap[id]);
		this._notifyExtensions(this.ON_REMOVED_EVENT, [id]);
    toRemoveTab.finalize();
  }
}

Tabs.prototype.addTabs = function addTabs(aTabsCollection) {
  if (aTabsCollection != null) {
    for (var i = 0, l = aTabsCollection.length; i < l; i++) {
      this._addTab(aTabsCollection.item(i));
    }
  }
}

Tabs.prototype.removeTabs = function removeTabs(aTabsCollection) {
  if (aTabsCollection != null) {
    for (var i = 0, l = aTabsCollection.length; i < l; i++) {
      this._removeTab(aTabsCollection.item(i));
    }
  }
}

Tabs.prototype._createTab = function _createTab(aWindowId, aIndex, aUrl, aSelected, aCallback) {
  var selectedWindow = null;
  if (aWindowId != null) {
    selectedWindow = this._windows.getWindow(aWindowId);
  }
  //TODO error if not selectedWindow ?
  if (selectedWindow == null) {
    selectedWindow = this._windows.getCurrentWindow();
  }
  var gBrowser = selectedWindow.getChromeWindow().gBrowser;
  var newTab = this._addTab(gBrowser.addTab(aUrl));
  var selected = aSelected == null ? true : aSelected;
  if (selected) {
    gBrowser.selectedTab = newTab.getChromeTab();
  }
  if (aIndex != null) {
    var tabContainer = gBrowser.tabContainer;
    var index = Math.max(Math.min(aIndex, tabContainer.itemCount), 0);
    gBrowser.moveTabTo(newTab.getChromeTab(), index);
  }
  //TODO to move to tab api
  if (aCallback != null) {
    try {
      aCallback.call(aCallback, newTab.getWrapper());
    }
    catch(e) {
      CONSOLE.error(e);
    }
  }
  return newTab;
}

Tabs.prototype.removeTab = function removeTab(aTabId, aCallback) {
  var tab = this._getTab(aTabId);
  if (tab == null) {
    //TODO Throw error ?
  }
  else {
    var chromeTab = tab.getChromeTab();
    chromeTab.ownerDocument.defaultView.gBrowser.removeTab(chromeTab);
    if (aCallback != null) {
      try {
        aCallback.call(aCallback);
      }
      catch(e) {
        CONSOLE.error(e);
      }
    }
  }
}

Tabs.prototype._getTabWrapper = function _getTabWrapper(aTabId) {
  var tab = this._getTab(aTabId);
  if (tab == null) {
    return null;
  }
  else {
    return tab.getWrapper();
  }
}

Tabs.prototype.getTabsWrappers = function getTabsWrappers(aWindowId) {
  var selectedWindow = null;
  if (aWindowId != null && aWindowId != undefined) {
    selectedWindow = this._windows.getWindow(aWindowId);
  }
  //TODO error if not selectedWindow ?
  if (selectedWindow == null) {
    selectedWindow = this._windows.getCurrentWindow();
  }
  var tabsWrapper = new Array();
  var gBrowser = selectedWindow.getChromeWindow().gBrowser;
  if (gBrowser != null) {
    var tabContainer = gBrowser.tabContainer;
    var currentSibling = tabContainer.firstChild;
    while (currentSibling != null) {
      var tab = this._findTab(currentSibling);
      if (tab != null) {
        tabsWrapper.push(tab.getWrapper());
      }
      currentSibling = currentSibling.nextSibling;
    }
  }
  return tabsWrapper;
}

Tabs.prototype._getTabForBrowser = function (aBrowser) {
  for (var id in this._tabsMap) {
    var tab = this._tabsMap[id];
    if (tab.getChromeTab().linkedBrowser == aBrowser) {
      return tab;
    }
  }
  return null;
}

Tabs.prototype._getSelectedTabWrapper = function _getSelectedTabWrapper(aWindowId) {
  var selectedWindow = null;
  if (aWindowId != null) {
    selectedWindow = this._windows.getWindow(aWindowId);
  }
  //TODO error if not selectedWindow ?
  if (selectedWindow == null) {
    selectedWindow = this._windows.getCurrentWindow();
  }
  var tabWrapper = null;
  var gBrowser = selectedWindow.getChromeWindow().gBrowser;
  if (gBrowser != null) {
    var tab = this._findTab(gBrowser.selectedTab);
    if (tab != null) {
      tabWrapper = tab.getWrapper();
    }
  }
  return tabWrapper;
}

Tabs.prototype._getSelectedTab = function _getSelectedTabWrapper() {
  var selectedWindow = this._windows.getCurrentWindow();
  var tab = null;
  var gBrowser = selectedWindow.getChromeWindow().gBrowser;
  if (gBrowser != null) {
    tab = this._findTab(gBrowser.selectedTab);
  }
  return tab;
}

Tabs.prototype._moveTab = function _moveTab(aTabId, aWindowId, aIndex, aCallback) {
  var tab = this._getTab(aTabId);
  if (tab == null) {
    //TODO Throw error ?
  }
  else {
    var selectedWindow = null;
    if (aWindowId != null) {
      selectedWindow = this._windows.getWindow(aWindowId);
    }
    else {
      selectedWindow = this._windows.getWindow(tab.getWindowId());
    }
    if (selectedWindow == null) {
      //TODO error if not selectedWindow ?
      return;
    }
    var gBrowser = selectedWindow.getChromeWindow().gBrowser;
    if (aWindowId != null && aWindowId != tab.getWindowId()) {
      var newTab = this._createTab(aWindowId, aIndex, "about:blank", true, null);
      //aOtherTab.linkedBrowser
      // var fieldsToSwap = [ "_docShell", "_webBrowserFind", "_contentWindow", "_webNavigation"];
      gBrowser.swapBrowsersAndCloseOther(newTab.getChromeTab(), tab.getChromeTab())
			//TODO move it to tabs
      if (aCallback != null) {
        try {
          aCallback.call(aCallback, newTab.getWrapper());
        }
        catch(e) {
          CONSOLE.error(e);
        }
      }
    }
    else {
      var tabContainer = gBrowser.tabContainer;
      var index = Math.max(Math.min(aIndex, tabContainer.itemCount), 0);
      gBrowser.moveTabTo(tab.getChromeTab(), index);
      if (aCallback != null) {
        try {
          aCallback.call(aCallback, tab.getWrapper());
        }
        catch(e) {
          CONSOLE.error(e);
        }
      }

    }


  }
}

Tabs.prototype._updateTab = function _updateTab(aTabId, aUrl, aSelected, aCallback) {
  var tab = this._getTab(aTabId);
  if (tab == null) {
    //TODO throw error?
  }
  else {
    if (aUrl != null) {
      tab.getChromeTab().linkedBrowser.loadURI(aUrl, null, null);
    }
    if (aSelected == true) {
      var gBrowser = tab.getChromeWindow().gBrowser;
      gBrowser.selectedTab = tab.getChromeTab();
    }
    if (aCallback != null) {
      try {
        aCallback.call(aCallback, tab.getWrapper());
      }
      catch(e) {
        CONSOLE.error(e);
      }
    }
  }
}

Tabs.prototype._captureVisibleTab = function _captureVisibleTab(aWindowId, aCallback) {
  var selectedWindow = null;
  if (aWindowId != null) {
    selectedWindow = this._windows.getWindow(aWindowId);
  }
  else {
    selectedWindow = this._windows.getCurrentWindow();
  }
  if (selectedWindow == null) {
    //TODO error if not selectedWindow ?
    return;
  }
  var chromeWindow = selectedWindow.getChromeWindow().gBrowser.selectedTab.linkedBrowser.contentWindow.window;
  try {
    aCallback.call(aCallback, HIDDENWINDOW.getSnapshot(chromeWindow));
  }
  catch(e) {
    CONSOLE.error(e);
  }
}

Tabs.prototype._onTabOpen = function _onTabOpen(aEvent) {
  this._addTab(aEvent.target);
}

Tabs.prototype._onTabMove = function _onTabMove(aEvent) {
  var tab = this._findTab(aEvent.target);
  var moveInfo = {
    windowId : tab.getWindowId(),
    fromIndex : aEvent.detail,
    toIndex : tab.getIndex()
  }
	this._notifyExtensions(this.ON_MOVED_EVENT, [tab.getID(), moveInfo]);
}

Tabs.prototype._onTabClose = function _onTabClose(aEvent) {
  this._removeTab(aEvent.target);
}

Tabs.prototype._onTabSelect = function _onTabSelect(aEvent) {
  var tab = this._findTab(aEvent.target);
  if (tab != null) {
		this._notifyExtensions(this.ON_SELECTION_CHANGED_EVENT, [tab.getID(), {
     windowId: tab.getWindowId()
     }]);
  }
}

Tabs.prototype.onLocationChange = function onLocationChange(aBrowser, webProgress, request, location) {
  var tab = this._getTabForBrowser(aBrowser);
  if (tab != null) {
    var window = webProgress.DOMWindow;
		this._notifyExtensions(this.ON_LOCATION_CHANGED_EVENT, [tab, window]);

    var onLoadHandler = {};
    onLoadHandler.callback = function(aTabs, aTab, aWindow, aOnLoadHandler) {
      return function() {
        aTabs._notifyExtensions(aTabs.ON_DOCUMENT_IDLE_EVENT, [aTab]);
        aWindow.removeEventListener("load", aOnLoadHandler.callback, true);
      }
    }(this, tab, window, onLoadHandler);

    window.addEventListener("load", onLoadHandler.callback, true);

    var onDOMContentLoadedHandler = {};
    onDOMContentLoadedHandler.callback = function(aTabs, aTab, aWindow, aOnDOMContentLoadedHandler) {
      return function() {
        aTabs._notifyExtensions(aTabs.ON_DOCUMENT_END_EVENT, [aTab]);
        aWindow.removeEventListener("DOMContentLoaded", aOnDOMContentLoadedHandler.callback, true);
      }
    }(this, tab, window, onDOMContentLoadedHandler);

    window.addEventListener("DOMContentLoaded", onDOMContentLoadedHandler.callback, true);

    var onDOMFrameContentLoadedHandler = {};
    onDOMFrameContentLoadedHandler.callback = function(aTabs, aTab, aWindow, aOnDOMFrameContentLoadedHandler) {
      return function() {
        aTabs._notifyExtensions(aTabs.ON_FRAMES_END_EVENT, [aTab]);
        aWindow.removeEventListener("DOMFrameContentLoaded", aOnDOMFrameContentLoadedHandler.callback, true);
      }
    }(this, tab, window, onDOMFrameContentLoadedHandler);

    window.addEventListener("DOMFrameContentLoaded", onDOMFrameContentLoadedHandler.callback, true);
    this._notifyExtensions(this.ON_DOCUMENT_START_EVENT, [tab]);
  }
}

Tabs.prototype.onProgressChange = function onProgressChange(aBrowser, webProgress, request, curSelfProgress, maxSelfProgress, curTotalProgress, maxTotalProgress) {

}

Tabs.prototype.onSecurityChange = function onSecurityChange(aBrowser, webProgress, request, state) {

}

Tabs.prototype.onStateChange = function onStateChange(aBrowser, webProgress, request, stateFlags, status) {
  if ((this._webProgressListenerIID.STATE_STOP & stateFlags) && (this._webProgressListenerIID.STATE_IS_WINDOW & stateFlags)) {
    var tab = this._getTabForBrowser(aBrowser);
    if (tab != null) {
      this._notifyExtensions(this.ON_STATE_CHANGED_EVENT, [tab]);
    }
  }
}

Tabs.prototype.onStatusChange = function onStatusChange(aBrowser, webProgress, request, aStatus, aMessage) {

}

Tabs.prototype.onRefreshAttempted = function onRefreshAttempted(aBrowser, webProgress, aRefreshURI, aMillis, aSameURI) {

}

Tabs.prototype.onLinkIconAvailable = function onLinkIconAvailable(aBrowser) {

}


Tabs.prototype.QueryInterface = function QueryInterface(aIID) {
  if (aIID.equals(Components.interfaces.nsIWebProgressListener) ||
      aIID.equals(Components.interfaces.nsISupportsWeakReference) ||
      aIID.equals(Components.interfaces.nsIObserver) ||
      aIID.equals(Components.interfaces.nsISupports))
    return this;
  throw Components.results.NS_NOINTERFACE;
}

Tabs.prototype.addProgressListener = function addProgressListener(aBrowser) {
  aBrowser.addTabsProgressListener(this);
  var container = aBrowser.tabContainer;
  this.addTabs(container.children);
  container.addEventListener("TabOpen", this.getTabOpenListener(), false);
  container.addEventListener("TabMove", this.getTabMoveListener(), false);
  container.addEventListener("TabClose", this.getTabCloseListener(), false);
  container.addEventListener("TabSelect", this.getTabSelectListener(), false);
}

Tabs.prototype.removeProgressListener = function removeProgressListener(aBrowser) {
  aBrowser.removeTabsProgressListener(this);
  var container = aBrowser.tabContainer;
  this.removeTabs(container.children);
  container.removeEventListener("TabOpen", this.getTabOpenListener(), false);
  container.removeEventListener("TabMove", this.getTabMoveListener(), false);
  container.removeEventListener("TabClose", this.getTabCloseListener(), false);
  container.removeEventListener("TabSelect", this.getTabSelectListener(), false);
}

Tabs.prototype._notifyExtensions = function _notifyExtensions(aEvent, aArgs){
  for(var id in this._extensions){
    var extension = this._extensions[id];
    extension.onNotification(this.SERVICENAME, aEvent, aArgs)
  }
}

var TABS = new Tabs().init();