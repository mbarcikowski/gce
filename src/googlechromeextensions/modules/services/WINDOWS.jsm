Components.utils.import("resource://googlechromeextensions/api/windows/Window.jsm");
Components.utils.import("resource://googlechromeextensions/services/CONSOLE.jsm");
Components.utils.import("resource://googlechromeextensions/services/HIDDENWINDOW.jsm");

var EXPORTED_SYMBOLS = ["WINDOWS"];

function Windows() {
  this._windowWatcher = null;
  this._windowsMap = {};
  this._currentWindowId = -1;
  this._windowWatcher = null;
  this._observerService = null;
  this._extensions = null;
  this._tabs = null;
}

Windows.prototype._windowwatcherCID = "@mozilla.org/embedcomp/window-watcher;1";
Windows.prototype._windowwatcherIID = Components.interfaces.nsIWindowWatcher;
Windows.prototype._observerserviceCID = "@mozilla.org/observer-service;1";
Windows.prototype._observerserviceIID = Components.interfaces.nsIObserverService;
Windows.prototype.SERVICENAME = "WINDOWS";
Windows.prototype.ON_REMOVED_EVENT = "ON_REMOVED_EVENT";
Windows.prototype.ON_NEW_WINDOW_EVENT = "ON_NEW_WINDOW_EVENT";
Windows.prototype.ON_CREATED_EVENT = "ON_CREATED_EVENT";
Windows.prototype.ON_FOCUS_CHANGED_EVENT = "ON_FOCUS_CHANGED_EVENT";

Windows.prototype.init = function init() {
  this._windowWatcher = Components.classes[this._windowwatcherCID].getService(this._windowwatcherIID);
  this._observerService = Components.classes[this._observerserviceCID].getService(this._observerserviceIID);
  this._lastFocusedWindow = null;
  this._observerService.addObserver(this, "xul-window-visible", false);
  this._windowWatcher.registerNotification(this);
  var enumerator = this._windowWatcher.getWindowEnumerator();
  while (enumerator.hasMoreElements()) {
    var window = enumerator.getNext().QueryInterface(Components.interfaces.nsIDOMWindow);
    this._observeWindow(window, null);
  }
  return this;
}

Windows.prototype.setTabs = function setTabs(aTabs){
  this._tabs = aTabs;
}

Windows.prototype.setExtensions = function setExtensions(aExtensions) {
  this._extensions = aExtensions;
}

Windows.prototype._setLastFocusedWindow = function _setLastFocusedWindow(aWindow) {
  this._lastFocusedWindow = aWindow;
}

Windows.prototype._getLastFocusedWindow = function _getLastFocusedWindow() {
  return this._lastFocusedWindow;
}

Windows.prototype._getNewWindowID = function _getNewWindowID() {
  return ++this._currentWindowId;
}

Windows.prototype._addWindow = function _addWindow(aWindow) {
  for (var id in this._windowsMap) {
    var window = this._windowsMap[id];
    if (window.getChromeWindow() == aWindow) {
      return;
    }
  }
  var id = this._getNewWindowID();
  var window = new Window().init(this, aWindow, id);
  this._windowsMap[id] = window;
  return window;
}

Windows.prototype._findWindow = function findWindow(aChromeWindow) {
  for (var id in this._windowsMap) {
    var window = this._windowsMap[id];
    if (window.getChromeWindow() == aChromeWindow) {
      return window;
    }
  }
  return null;
}


Windows.prototype.findWindowId = function findWindowId(aWindow) {
  for (var id in this._windowsMap) {
    var window = this._windowsMap[id];
    if (window.getChromeWindow() == aWindow) {
      return id;
    }
  }
  return null;
}

Windows.prototype._removeWindow = function _removeWindow(aWindow) {
  if (aWindow.gBrowser == null) {
    return;
  }
  var toRemoveWindow = null;
  for (var id in this._windowsMap) {
    var window = this._windowsMap[id];
    if (window.getChromeWindow() == aWindow) {
      toRemoveWindow = window;
    }
  }
  if (toRemoveWindow != null) {
    var id = toRemoveWindow.getID();
    delete (this._windowsMap[id]);
    this._notifyExtensions(this.ON_REMOVED_EVENT, [id]);
    this._tabs.removeProgressListener(toRemoveWindow.getChromeWindow().gBrowser);
    toRemoveWindow.finalize();
  }
}

Windows.prototype._createWindow = function _createWindow(aUrl, aLeft, aTop, aWidth, aHeight, aCallback) {
  this._observeWindow(this._openWindow(aUrl, aLeft, aTop, aWidth, aHeight), aCallback);
}

Windows.prototype._openWindow = function (aUrl, aLeft, aTop, aWidth, aHeight) {
  return HIDDENWINDOW.openWindow(aUrl, aLeft, aTop, aWidth, aHeight);
}

Windows.prototype._removeWindowId = function _removeWindowId(aWindowId, aCallback) {
  var toRemoveWindow = this._windowsMap[aWindowId];
  if (toRemoveWindow != null) {
    toRemoveWindow.getChromeWindow().close();
    if (aCallback) {
      try {
        aCallback();
      }
      catch(e) {
        CONSOLE.error(e);
      }
    }
  }
}

Windows.prototype._updateWindowId = function _updateWindowId(aWindowId, aLeft, aTop, aWidth, aHeight, aCallback) {
  var toUpdateWindow = this._windowsMap[aWindowId];
  if (toUpdateWindow != null) {
    var chromewindow = toUpdateWindow.getChromeWindow();
    if (aLeft != null || aTop != null) {
      chromewindow.moveTo((aLeft != null ? aLeft : chromewindow.left), (aTop != null ? aTop : chromewindow.top));
    }
    if (aWidth != null || aHeight != null) {
      chromewindow.resizeTo((aWidth != null ? aWidth : chromewindow.width), (aHeight != null ? aHeight : chromewindow.height));
    }
    if (aCallback) {
      try {
        aCallback(toUpdateWindow.getWrapper());
      }
      catch(e) {
        CONSOLE.error(e);
      }
    }
  }
}

Windows.prototype._observeWindow = function _observeWindow(aChromeWindow, aCallBack) {

  var listener = {};
  listener.callback = function(aWindows, aCallBack2) {
    return function () {
      aChromeWindow.removeEventListener("load", listener, false);
      if (aChromeWindow.gBrowser != null) {
        aWindows._notifyExtensions(aWindows.ON_NEW_WINDOW_EVENT, [aChromeWindow]);
        var window = aWindows._addWindow(aChromeWindow);
        if (window != null) {
          aWindows._setLastFocusedWindow(window);
          aWindows._tabs.addProgressListener(aChromeWindow.gBrowser);
          var wrapper = window.getWrapper();
          if (aCallBack2) {
            try {
              aCallBack2(wrapper);
            }
            catch(e) {
              CONSOLE.error(e);
            }
          }
          aWindows._notifyExtensions(aWindows.ON_CREATED_EVENT, [wrapper]);
          aWindows._notifyExtensions(aWindows.ON_FOCUS_CHANGED_EVENT, [window.getID()]);
        }
      }
    }
  }(this, aCallBack);
  aChromeWindow.addEventListener("load", listener.callback, false);

}

Windows.prototype.observe = function observe(aSubject, aTopic, aData) {
  switch (aTopic) {
    case "domwindowopened" :
      this._observeWindow(aSubject, null);
      break;
    case "domwindowclosed" :
      this._removeWindow(aSubject);
      break;
    case "xul-window-visible" :
      var lastFocusedWindow = this._getLastFocusedWindow();
      var chromeWindow = this._windowWatcher.activeWindow;
      if (chromeWindow != null && chromeWindow.gBrowser != null && (lastFocusedWindow == null || chromeWindow != lastFocusedWindow.getChromeWindow())) {
        var window = this._findWindow(chromeWindow);
        if (window != null) {
          this._setLastFocusedWindow(window);
          this._notifyExtensions(this.ON_FOCUS_CHANGED_EVENT, [window.getID()]);
        }
      }
      break;
  }
}

Windows.prototype.getCurrentWindow = function getCurrentWindow() {
  return this._getLastFocusedWindow();
}

Windows.prototype.getWindow = function getWindow(aId) {
  return this._windowsMap[aId];
}

Windows.prototype._getWindowWrapper = function _getWindowWrapper(aId) {
  var window = this._windowsMap[aId];
  if (window != null) {
    return window.getWrapper();
  }
  return null;
}

Windows.prototype._getAllWindowWrappers = function _getAllWindowWrappers(aPopulate) {
  var windowWrappers = new Array();
  if (aPopulate == true) {
    for (var id in this._windowsMap) {
      var window = this._windowsMap[id];
      var wrapper = window.getWrapper();
      var tabs = this._tabs.getTabsWrappers(id);
      wrapper.__defineGetter__("tabs", function() {
        return tabs;
      });
      windowWrappers.push(wrapper);
    }
  }
  else {
    for (var id in this._windowsMap) {
      windowWrappers.push(this._windowsMap[id].getWrapper());
    }
  }
  return windowWrappers;
}

Windows.prototype._notifyExtensions = function _notifyExtensions(aEvent, aArgs){
  for(var id in this._extensions){
    var extension = this._extensions[id];
    extension.onNotification(this.SERVICENAME, aEvent, aArgs)
  }
}

try{
var WINDOWS = new Windows().init();
}
catch(e){
  CONSOLE.error(e);
}